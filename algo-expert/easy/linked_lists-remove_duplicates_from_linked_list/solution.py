# This is an input class. Do not edit.
class LinkedList:
    def __init__(self, value):
        self.value = value
        self.next = None


def removeDuplicatesFromLinkedList(linkedList):
    curr = linkedList
    while curr:
        next = curr.next
        while next and next.value == curr.value:
            next = next.next
        curr.next = next
        curr = curr.next
    return linkedList