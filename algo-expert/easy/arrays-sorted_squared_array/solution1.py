def sortedSquaredArray(array):
    res = []
    for num in array:
        res.append(num ** 2)
    res.sort()
    return res