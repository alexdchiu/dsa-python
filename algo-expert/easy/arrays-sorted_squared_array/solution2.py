def sortedSquaredArray(array):
    sorted_res = [0 for num in array]
    small = 0
    big = len(array) - 1

    for idx in range(len(array)-1, -1, -1):
        smallVal = array[small]
        bigVal = array[big]

        if abs(smallVal) > abs(bigVal):
            sorted_res[idx] = smallVal**2
            small += 1
        else:
            sorted_res[idx] = bigVal ** 2
            big -= 1
    return sorted_res