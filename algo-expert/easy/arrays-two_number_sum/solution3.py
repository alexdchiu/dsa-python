def twoNumberSum(array, targetSum):
    left = 0
    right = len(array) - 1
    array.sort()
    while left < right:
        currSum = array[left] + array[right]
        if targetSum == currSum:
            return [array[left], array[right]]
        elif currSum < targetSum:
            left += 1
        elif currSum > targetSum:
            right -= 1
    return []