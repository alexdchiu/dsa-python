def twoNumberSum(array, targetSum):
    hash = {}
    for num in array:
        y = targetSum - num
        if y in hash:
            return [num, y]
        else:
            hash[num] = True
    return []
