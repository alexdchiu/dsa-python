def twoNumberSum(array, targetSum):
    for i in range (len(array)):
        for j in range (i+1, len(array)):
            if array[i] + array[j] == targetSum:
                return [array[i], array[j]]
    return []

test = {
  "array": [3, 5, -4, 8, 11, 1, -1, 6],
  "targetSum": 10
}

# {
#   "array": [-7, -5, -3, -1, 0, 1, 3, 5, 7],
#   "targetSum": -5
# }

# {
#   "array": [-21, 301, 12, 4, 65, 56, 210, 356, 9, -47],
#   "targetSum": 163
# }

# {
#   "array": [-21, 301, 12, 4, 65, 56, 210, 356, 9, -47],
#   "targetSum": 164
# }

twoNumberSum(test["array"], test["targetSum"])