def isValidSubsequence(array, sequence):
    seq_id = 0
    for num in array:
        if sequence[seq_id] == num:
            seq_id += 1
        if seq_id == len(sequence):
            break
    return seq_id == len(sequence)