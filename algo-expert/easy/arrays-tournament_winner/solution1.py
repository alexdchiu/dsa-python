def tournamentWinner(competitions, results):
    points_obj = {}
    for i in range (len(competitions)):
        home = competitions[i][0]
        if home not in points_obj:
            points_obj[home] = 0
        away = competitions[i][1]
        if away not in points_obj:
            points_obj[away] = 0
        points_obj[home] += (results[i] * 3)
        points_obj[away] += ((1-results[i]) * 3)

    return max(points_obj, key = points_obj.get)