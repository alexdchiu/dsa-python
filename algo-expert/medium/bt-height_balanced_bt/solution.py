class BinaryTree:
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right

class TreeInfo:
    def __init__(self, is_balanced, height):
        self.is_balanced = is_balanced
        self.height = height

def heightBalancedBinaryTree(tree):
    tree_info = get_tree_info(tree)
    return tree_info.is_balanced

def get_tree_info(node):
    if node is None:
        return TreeInfo(True, 0)

    left = get_tree_info(node.left)
    right = get_tree_info(node.right)

    is_balanced = left.is_balanced and right.is_balanced and abs(
        left.height - right.height) <= 1

    height = max(left.height, right.height) + 1

    return TreeInfo(is_balanced, height)