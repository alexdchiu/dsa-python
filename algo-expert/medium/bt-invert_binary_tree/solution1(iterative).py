def invertBinaryTree(tree):
    queue = [tree]

    while len(queue) > 0:
        curr = queue.pop(0)

        if curr is None:
            continue
        queue.append(curr.left)
        queue.append(curr.right)
        switch_nodes(curr)

def switch_nodes(tree):
    tree.left, tree.right = tree.right, tree.left


# This is the class of the input binary tree.
class BinaryTree:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None