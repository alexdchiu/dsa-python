# This is an input class. Do not edit.
class BinaryTree:
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


def binaryTreeDiameter(tree):
    return getTreeInfo(tree).diameter


def getTreeInfo(tree):
    if tree is None:
        return TreeInfo(0,0)

    leftTreeInfo = getTreeInfo(tree.left)
    rightTreeInfo = getTreeInfo(tree.right)

    longestPathThroughRoot = leftTreeInfo.height + rightTreeInfo.height
    maxDiameterSoFar = max(leftTreeInfo.diameter, rightTreeInfo.diameter)
    currDiameter = max(longestPathThroughRoot, maxDiameterSoFar)
    currHeight = 1 + max(leftTreeInfo.height, rightTreeInfo.height)

    return TreeInfo(currDiameter, currHeight)

class TreeInfo: # acts as return type for recursive function
    # allows us to return object which is an instance of TreeInfo 
    # we can access the diameter and height directly -> easier to read 
    def __init__(self, diameter, height):
        self.diameter = diameter
        self.height = height