def hasSingleCycle(array):
    num_elements_visited = 0
    curr_idx = 0
    while num_elements_visited < len(array):
        if num_elements_visited > 0 and curr_idx == 0:
            return False
        num_elements_visited += 1
        curr_idx = get_next_idx(curr_idx, array)
    return curr_idx == 0

def get_next_idx(curr_idx, array):
    jump = array[curr_idx]
    next_idx = (curr_idx + jump) % len(array)
    return next_idx