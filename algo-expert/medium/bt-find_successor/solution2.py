# This is an input class. Do not edit.
class BinaryTree:
    def __init__(self, value, left=None, right=None, parent=None):
        self.value = value
        self.left = left
        self.right = right
        self.parent = parent


def findSuccessor(tree, node):
    if node.right:
        return get_left_most_child(node.right)

    return get_right_most_parent(node)

def get_left_most_child(node):
    curr_node = node
    while curr_node.left:
        curr_node = curr_node.left

    return curr_node

def get_right_most_parent(node):
    curr_node = node
    while curr_node.parent and curr_node.parent.right == curr_node:
        curr_node = curr_node.parent

    return curr_node.parent

