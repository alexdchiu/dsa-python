# This is an input class. Do not edit.
class BinaryTree:
    def __init__(self, value, left=None, right=None, parent=None):
        self.value = value
        self.left = left
        self.right = right
        self.parent = parent


def findSuccessor(tree, node):
    in_order_traversal_order = get_in_order_traversal_order(tree)

    for idx, curr_node in enumerate(in_order_traversal_order):
        if curr_node != node:
            continue
        if idx == len(in_order_traversal_order) - 1:
            return None

        return in_order_traversal_order[idx + 1]

def get_in_order_traversal_order(node, order = []):
    if node is None:
        return order

    get_in_order_traversal_order(node.left, order)
    order.append(node)
    get_in_order_traversal_order(node.right, order)

    return order