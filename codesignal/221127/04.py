def solution(queryType, query):
    max_length = 0
    add_to_key = 0
    for i in range(len(queryType)):
        if queryType[i] == "insert":
            # print(queryType[i], query[i])
            max_length = max(max_length, query[i][1])
        if queryType[i] == "addToKey":
            # print(queryType[i], query[i])
            add_to_key += query[i][0]
    max_length += add_to_key    
    track = [0 for i in range(max_length + 1)]
    # print(track)
    res = 0
    add_to_key = 0
    for i in range(len(queryType)):
        if queryType[i] == "insert":
            k, v = query[i]
            track[k] += v
        if queryType[i] == "addToValue":
            # print(queryType[i], query[i])
            for j in range(len(track)):
                if track[j] > 0:
                    track[j] += query[i][0]
        if queryType[i] == "addToKey":
            # print(queryType[i], query[i])
            # add_to_key += query[i][0]
            add_to_key = max(add_to_key + query[i][0],0)
        if queryType[i] == "get":
            idx = query[i][0] - add_to_key
            res += track[idx]
            # print(res)
    # print(track)
    return res