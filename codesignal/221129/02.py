def solution(a):
    l, r = 0, len(a)-1
    b = []
    last = -float('inf')
    
    while l <= r:
        if a[l] <= last:
            return False
        b.append(a[l])
        last = a[l]
        if l == r:
            break
        if a[r] <= last:
            return False
        b.append(a[r])
        last = a[r]
        l += 1
        r -= 1
    return True



a: [1, 3, 5, 6, 4, 2]
Expected Output:
true

a: [1, 4, 5, 6, 3]
Expected Output:
false

a: [1]
Expected Output:
true

a: [1, 2]
Expected Output:
true

a: [-89, -47, -38, 39, 82, 87, 40, -9, -41, -68]
Expected Output:
true

a: [-92, -17, 71, 76, 54, -35]
Expected Output:
true

a: [-52, 2, 31, 56, 47, 29, -35]
Expected Output:
true

a: [-97, -51, -8, 25, 44, 70, 98, 77, 68, 31, -5, -36, -80]
Expected Output:
true

a: [-86, -49, -26, -22, 22, 32, 44, 67, 38, 25, 13, -25, -42, -71]
Expected Output:
true


a: [-92, -23, 0, 45, 89, 96, 99, 95, 89, 41, -17, -48]
Expected Output:
false

a: [-37, -31, -8, 88, -10, -33]
Expected Output:
true

a: [-87, -52, 83, 96, 98, 94, 68, -71]
Expected Output:
true

a: [-92, 9, 41, 99, 29, -78]
Expected Output:
true

a: [-91, -84, -67, -44, 9, 70, 88, 37, -11, -67, -72, -87]
Expected Output:
false


a: [-44, -73, -24, 22, 91, 84, 2, -40, -56]
Expected Output:
false

a: [-79, -48, -42, 4, 9, 55, 70, 84, 62, 40, 7, -28, -46, -74]
Expected Output:
true

a: [-27, -81, -7, 21, 33, 52, 23, -6, -10, -71]
Expected Output:
false

a: [-99, -29, -7, 17, 28, 71, 98, 86, 42, 22, 0, -29, -86]
Expected Output:
false

a: [-86, 93, 4, 27, 49, 74, 74, -70, 60, 41, 14, -44, -80]
Expected Output:
false

a: [42, -80, -72, -25, 18, -94, 99, 49, 37, 10, -45, -76, -87]
Expected Output:
false

a: [-73, -27, 52, 60, 90, 91, 71, 55, 30, -56]
Expected Output:
true


a: [-98, -82, -70, -49, 58, 26, -69, -79, -98]
Expected Output:
false