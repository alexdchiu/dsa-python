def solution(numbers, left, right):
    res = []
    for i in range(len(numbers)):
        x = numbers[i] / (i + 1)
        if numbers[i] % (i + 1) != 0:
            res.append(False)
        else:
            if left <= x and x <= right:
                res.append(True)
            else:
                res.append(False)
    return res


numbers: [8, 5, 6, 16, 5]
left: 1
right: 3
Expected Output:
[false, false, true, false, true]

numbers: [100]
left: 1
right: 1000
Expected Output:
[true]


numbers: [1, 2, 3, 4, 5]
left: 1
right: 1
Expected Output:
[true, true, true, true, true]

numbers: [1, 2, 3, 4, 5]
left: 2
right: 10000
Expected Output:
[false, false, false, false, false]

numbers: [1000000, 20000]
left: 10000
right: 10000
Expected Output:
[false, true]

numbers: [65, 46, 60, 164, 243, 228, 231, 298, 231, 211]
left: 20
right: 50
Expected Output:
[false, true, true, true, false, true, true, false, false, false]

numbers: [643, 531, 504, 224, 415, 360, 364, 84, 212, 587]
left: 70
right: 80
Expected Output:
[false, false, false, false, false, false, false, false, false, false]

numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49]
left: 1
right: 10000
Expected Output:
[true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true]

