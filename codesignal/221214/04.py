# To solve this problem in O(n) time, we can use a different approach. Instead of computing the sum of each pair of numbers and checking if it is a full square, we can precompute the full squares up to a certain maximum value, then for each number in the array, we can check how many other numbers in the array can be added to it to form a full square.

# Here is some sample code for this solution:

import math
def solution(numbers):
    copy = numbers[:]
    copy.sort(reverse = True)
    max_sum = copy[0] + copy[0]
    iterate_to = math.ceil(math.sqrt(max_sum))
    squares = set()
    i = 0
    while i <= iterate_to:
        squares.add(i**2)
        i += 1
    count = {}
    for num in numbers:
        if num not in count:
            count[num] = 0
        count[num] += 1
    # print(count)
    # print(squares)
    res = 0
    for n in numbers:
        for sq in squares:
            tgt = sq - n
            # print(tgt)
            if tgt in count:
                res += 1
                # print(n, tgt)
        count[n] -= 1
        if count[n] == 0:
            del count[n]
        
    return res


numbers: [-1, 18, 3, 1, 5]
Expected Output:
4



numbers: [2]
Expected Output:
1


numbers: [-2, -1, 0, 1, 2]
Expected Output:
6


numbers: [-20000, 20000, 0, 1, 4, 9, 16]
Expected Output:
8

numbers: [-7, -2, -5, 5, -9, 8, -4, 1]
Expected Output:
6


numbers: [-6, -9, -7, -5, 6, 2, 7, 5, -14, 10, -8, 1, 4]
Expected Output:
11


numbers: [-16, 7, -12, -1, 1, 20, -3, -10, -6, 5, -18, 10, -11, 2, -15, 19, -2, 14, -19, -5, -9]
Expected Output:
25


numbers: [16, 26, 12, 22, -6, 8, 18, 4, 3, -7, -24, 14, 15, -4, -29, 27, -26, 11, -2, -11, 30, -10, 0, -14, -8, -25, 20, -28, -17, 6, 2, 17, -18, -1]
Expected Output:
60


numbers: [-7, -36, 6, -28, -34, 16, -47, -6, 49, -4, -8, 0, 15, 24, -26, -40, 25, -14, -37, 44, 10, 47, 33, 9, 26, 34, -48, 8, -19, -9, 29, 21, -5, 5, 11, 37, 13, -44, 42, -38, -30, 45, -35, -27, 1, -16, 4, 48, 2, -15, -18, 43, -17, 27, 7]
Expected Output:
112


numbers: [-56, 31, 33, 55, 94, -95, -20, 41, -77, 64, -11, -34, -90, -8, 15, 86, 39, 60, 8, -96, 44, 91, -81, 100, -98, -45, -9, -70, 17, 85, -91, -62, -53, -63, 45, 37, -7, 47, -15, 56, 77, -1, -24, 68, 72, 3, 71, -37, 6, -17, -49, 29, -5, -44, 98, -42, -84, 0, -74, -57, -19, 18, 25, -38, -31, 42, 35, 28, 23, -39, 83, -79, -30, -2, -41, -43, -25, 97, 80, 11, 49, 21, 90, 95, -93, 20, -26, -23, 7]
Expected Output:
203