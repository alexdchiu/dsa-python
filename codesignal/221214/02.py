import itertools

def solution(numbers):
    count = 0
    for i in range(len(numbers)-1):
        if numbers[i+1] <= numbers[i]:
            count += 1
            if count > 1:
                return False
    return True



import itertools

def solution(numbers):
    count = 0
    for i in range(len(numbers)-1):
        if numbers[i+1] <= numbers[i]:
            count += 1
            if count > 1:
                return False
            for s in f(numbers[i+1]):
                return False if (ele <= numbers[i])
    return True

def f(num):
    res = []
    s = str(num)
    for idx1, idx2 in itertools.combinations(range(len(s)),2):
        swapped_s = list(s)
        swapped_s[idx1], swapped_s[idx2] = swapped_s[idx2], swapped_s[idx1]
        res.append(''.join(swapped_s))
    return res


numbers: [1, 5, 10, 20]
Expected Output:
true


numbers: [1, 3, 900, 10]
Expected Output:
true


numbers: [13, 31, 30]
Expected Output:
false

numbers: [111]
Expected Output:
true

numbers: [1000, 10, 100]
Expected Output:
true

numbers: [527, 516, 216, 965, 951]
Expected Output:
false

numbers: [68, 105, 131, 396, 438, 754, 744, 817]
Expected Output:
true

numbers: [92, 121, 193, 293, 328, 345, 343, 475, 478, 154, 250, 706, 929]
Expected Output:
false

numbers: [64, 281, 219, 239, 291, 299, 308, 352, 371, 421, 405, 497, 875, 648, 725, 832, 877, 911, 925, 929, 954]
Expected Output:
false

numbers: [43, 46, 68, 79, 94, 109, 131, 140, 172, 192, 193, 195, 426, 294, 302, 359, 436, 439, 517, 520, 607, 619, 692, 807, 714, 753, 796, 803, 807, 879, 890, 899, 945, 962]
Expected Output:
false