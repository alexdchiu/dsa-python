
def solution(numbers):
    res = []
    for i in range(len(numbers)-2):
        a = numbers[i]
        b = numbers[i+1]
        c = numbers[i+2]
        zig = a < b > c
        zag = a > b < c
        if zig or zag:
            res.append(1)
        else:
            res.append(0)
            
    return res



# numbers: [1, 2, 1, 3, 4]
# Expected Output:
# [1, 1, 0]



# numbers: [1, 2, 3, 4]
# Expected Output:
# [0, 0]


# numbers: [1000000000, 1000000000, 1000000000]
# Expected Output:
# [0]


# numbers: [1, 2, 4, 3, 1]
# Expected Output:
# [0, 1, 0]


# numbers: [3, 5, 2, 6, 10]
# Expected Output:
# [1, 1, 0]


# numbers: [1, 3, 4, 5, 6, 14, 14]
# Expected Output:
# [0, 0, 0, 0, 0]


# numbers: [1, 5, 7, 3, 10, 2, 4, 9, 8, 6]
# Expected Output:
# [0, 1, 1, 1, 1, 0, 1, 0]


# numbers: [11, 14, 3, 17, 16, 13, 3, 7, 19, 8]
# Expected Output:
# [1, 1, 1, 0, 0, 1, 0, 1]