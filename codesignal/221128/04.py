# import math

# def solution(a, k):
#     # res = 0
#     # for i in range(len(a)):
#     #     for j in range(i+1, len(a)):
#     #         curr = a[i] + a[j]
#     #         if curr % k == 0:
#     #             res += 1
#     # return res
#     remainders = [a[i] % k for i in range(len(a))]
#     # print(remainders)
#     num_of_remainders = {}
#     for remainder in remainders:
#         if remainder not in num_of_remainders:
#             num_of_remainders[remainder] = 0
#         num_of_remainders[remainder] += 1
#     # print(num_of_remainders)
#     res = 0
#     for remainder in remainders:
#         curr = remainder
#         opposite = k - curr
#         num_of_remainders[curr] -= 1
#         if curr == 0:
#             res += num_of_remainders[curr]
#         else:
#             res += num_of_remainders[opposite]
#     return res


def solution(a,k):

    remainders = [a[i] % k for i in range(len(a))]
    num_of_remainders = {}
    for remainder in remainders:
        if remainder not in num_of_remainders:
            num_of_remainders[remainder] = 0
        num_of_remainders[remainder] += 1
    res = 0
    # print(num_of_remainders)
    for remainder in remainders:
        curr = remainder
        opposite = k - curr
        num_of_remainders[curr] -= 1
        if curr == 0:
            res += num_of_remainders[curr]
        else:
            res += num_of_remainders[opposite]
        # print(curr, opposite, num_of_remainders, 'res', res)
    # return res
    print(res)

# solution([2, 2, 2], 2)

# # FROM EMMA

# def solution(arr, k):
#     d = {}
    
#     count = 0 
#     for i in range(len(arr)):
#         remainder = arr[i] % k
#         if k - remainder in d:
#             count += d[k - remainder]
#         if remainder in d:
#             d[remainder] += 1
#         else:
#             d[remainder] = 1
#     print(count)


a = [1, 2, 3, 4, 5]
k = 3
solution(a, k)
# Expected Output:
# 4

a = [1, 3, 5, 7, 9]
k = 2
solution(a, k)
# Expected Output:
# 10

a = [1000000000]
k = 1
solution(a, k)
# Expected Output:
# 0

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
k = 5
solution(a, k)
# Expected Output:
# 9

a = [857910, 819589, 924892, 903743, 258550, 372448, 806750, 766606, 990597, 206447]
k = 1
solution(a, k)
# Expected Output:
# 45

a = [51, 24, 44, 87, 64, 84, 76, 47, 31, 21, 34, 49, 30, 70, 83, 3, 38, 6, 99, 7, 92, 59, 57, 50, 73, 79, 93, 19, 68, 72, 23, 90, 71, 35, 77, 28, 36, 45, 94, 29, 39, 98, 48, 9, 60, 20, 62, 100, 1, 97, 16, 66, 56, 54, 80, 15, 46, 18, 2, 88, 8, 65, 69, 78, 40, 37, 33, 25, 89, 13, 85, 5, 32, 86, 11, 52, 63, 67, 12, 55, 82, 43, 81, 91, 26, 27, 95, 58, 75, 61, 41, 17, 14, 10, 96, 53, 4, 22, 74, 42]
k = 10
solution(a,k)
# Expected Output:
# 490
