def solution(s):
    if len(s) == 0:
        return s
    l, r = 0, len(s) - 1
    while l < r:
        print(s[l:r+1])
        curr = s[l:r+1]
        reverse = curr[::-1]
        if curr == reverse:
            print(curr, reverse, l, r)
            s = s[r+1:]
            l, r = 0, len(s)
        r -= 1
    return s   


s: "aaacodedoc"
Expected Output:
""

s: "codesignal"
Expected Output:
"codesignal"

s: ""
Expected Output:
""

s: "a"
Expected Output:
"a"

s: "abbab"
Expected Output:
"b"

s: "aaabba"
Expected Output:
"a"

s: "aaaaaaab"
Expected Output:
"b"


s: "abbaabbaabba"
Expected Output:
""

s: "abababaaab"
Expected Output:
"b"

s: "bbabbaabaabbbbb"
Expected Output:
""