def solution(numbers, left, right):
    res = []
    print(left, right)
    for i in range(len(numbers)):
        print(numbers[i], (i + 1))
        if (numbers[i] % (i + 1)) != 0:
           res.append(False)
           print(False)
        elif (numbers[i] / (i + 1)) > right or (numbers[i] / (i + 1)) < left:
            res.append(False)
            print(False)
        else:
            res.append(True)
            print(True)
    return res


def solution(numbers):
    prev = 0
    print(numbers)
    for i in range(1,len(numbers)-1):
        curr = i
        nxt = i + 1
        if numbers[curr] > numbers[prev] and numbers[curr] < numbers[nxt]:
            prev = curr
        else:
            lower, upper = numbers[prev], numbers[nxt]
            curr_digits = []
            for digit in str(numbers[curr]):
                curr_digits.append(digit)
            curr_digits.sort()
            # print(curr_digits)            
            # curr_number = [0 for i in range(len(curr_digits))]
            for i in range(len(curr_digits)):
                for j in range(i+1, len(curr_digits)):
                    curr_digits[i], curr_digits[j] = curr_digits[j], curr_digits[i]
                    new_num = ''.join(curr_digits)
                    between = lower < int(new_num) < upper
                    
            
            
            
            prev = curr
    return True


# def scramble(prev, curr, nxt):
#     for digit in str(curr):
#         print(digit)


from collections import deque

def solution(field, x, y):
    visited = set()
    res = [[-1 for x in range(len(field))] for y in range(len(field[0]))]
    neighbors = [
        (1,1),
        (1,0),
        (1,-1),
        (0,1),
        (0,-1),
        (-1,1),
        (-1,0),
        (-1,-1)
    ]
    
    queue = deque([(x,y)])
    
    while queue:
        curr = queue.popleft()
        
        if curr in visited:
            return res
        
        visited.add(curr)
        
        for neighbor in neighbors:
            dx, dy = neighbor
            new_x, new_y = x + dx, y + dy
            new_pos = (new_x, new_y)
            x_inbounds = 0 <= new_x < len(field)
            y_inbounds = 0 <= new_y < len(field)
            if 
            queue.append(new_pos)


def solution(a, k):
    max_size = 0
    res = 0
    for i in range(len(a)):
      if a[i] > max_size:
          max_size = a[i]
    for l in range(1,max_size+1):
        count = 0
        # print('\n', 'new', l)
        for m in range(len(a)):
            count += a[m] // l
            if count >= k:
                res = l
                # print('break', res)
                break
    return res