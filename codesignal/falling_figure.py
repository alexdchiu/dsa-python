def solution(field, figure):
  height = len(field)
  width = len(field[0])
  figure_size = len(figure)

  for col in range(width - figure_size + 1):
    row = 1
    while row < height - figure_size + 1:
      can_fit = True
      for dx in range(figure_size):
        for dy in range(figure_size):
          if field[row + dx][col + dy] == 1 and figure[dx][dy] == 1:
            can_fit = False
      if not can_fit:
        break
      row += 1
    row -= 1
    
    for dx in range(figure_size):
      row_filled = True
      for col_idx in range(width):
        if not (field[row+dx][col_idx] == 1 or (col <= col_idx < col + figure_size and figure[dx][col_idx - col] == 1)):
          row_filled = False
      if row_filled:
        # return col
        print(col)
  print(-1)
  # return -1


field = [
  [0,0,0,0,0,0],
  [0,0,0,0,0,0],
  [0,0,0,0,0,0],
  [1,0,0,0,0,0],
  [1,1,0,0,0,1],
  [1,1,0,0,0,1],
  [1,1,1,0,1,1],
]

figure = [
  [1,1,1],
  [0,1,1],
  [0,1,0],
]

solution(field, figure)
