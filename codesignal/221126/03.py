from collections import deque

def solution(field, x, y):
    visited = set()
    res = [[-1 for x in range(len(field))] for y in range(len(field[0]))]
    count = explore(field, x, y, visited)
    res[x][y] = count
    return res
    

def explore(field, x, y, visited):
    curr = (x,y)
    if curr in visited:
        return 0
        
    if curr not in visited:
        visited.add(curr)
    
    if field[x][y] == True:
        return 1
    
    deltas = [
        (-1,-1),
        (-1,0),
        (-1,1),
        (0,-1),
        (0,1),
        (1,-1),
        (1,0),
        (1,1)
    ]
    
    count = 0
    
    queue = deque([curr])

    while queue:
      curr = deque.popleft()
      x,y = curr

      for delta in deltas:
          dx, dy = delta
          new_x, new_y = (x + dx), (y + dy)
          new_pos = (new_x, new_y)
          x_inbounds = 0 <= new_x < len(field)
          y_inbounds = 0 <= new_y < len(field[0])
          if x_inbounds and y_inbounds and new_pos not in visited:
              count += explore(field, new_x, new_y, visited)
            
    
    return count


field:
[[false,true,true], 
 [true,false,true], 
 [false,false,true]]
x: 1
y: 1

Expected Output:
[[-1,-1,-1], 
 [-1,5,-1], 
 [-1,-1,-1]]

field:
[[true,false,true,true,false], 
 [true,false,false,false,false], 
 [false,false,false,false,false], 
 [true,false,false,false,false]]
x: 3
y: 2

Expected Output:
[[-1,-1,-1,-1,-1], 
 [-1,3,2,2,1], 
 [-1,2,0,0,0], 
 [-1,1,0,0,0]]

field:
[[false,true], 
 [false,false]]
x: 0
y: 0

Expected Output:
[[1,-1], 
 [-1,-1]]


field:
[[true,false,false], 
 [false,true,false]]
x: 0
y: 1

Expected Output:
[[-1,2,-1], 
 [-1,-1,-1]]


field:
[[false,false,false], 
 [false,false,false], 
 [false,false,false], 
 [false,true,true], 
 [false,false,false]]
x: 1
y: 0

Expected Output:
[[0,0,0], 
 [0,0,0], 
 [1,2,2], 
 [-1,-1,-1], 
 [-1,-1,-1]]


field:
[[false,false,false,false,false,false], 
 [false,false,false,false,false,false], 
 [false,false,false,false,false,false], 
 [false,false,false,false,false,false]]
x: 2
y: 5

Expected Output:
[[0,0,0,0,0,0], 
 [0,0,0,0,0,0], 
 [0,0,0,0,0,0], 
 [0,0,0,0,0,0]]


field:
[[false,false,true], 
 [true,false,true], 
 [false,true,false], 
 [true,false,false], 
 [false,true,false], 
 [false,true,false]]
x: 0
y: 0


Expected Output:
[[1,-1,-1], 
 [-1,-1,-1], 
 [-1,-1,-1], 
 [-1,-1,-1], 
 [-1,-1,-1], 
 [-1,-1,-1]]


field:
[[false,false,true,false], 
 [false,false,false,false], 
 [true,false,false,false], 
 [false,true,false,false], 
 [false,false,true,false], 
 [false,false,false,false], 
 [false,false,false,false], 
 [false,false,false,false]]
x: 3
y: 0

Expected Output:
[[-1,-1,-1,-1], 
 [-1,-1,-1,-1], 
 [-1,-1,-1,-1], 
 [2,-1,-1,-1], 
 [-1,-1,-1,-1], 
 [-1,-1,-1,-1], 
 [-1,-1,-1,-1], 
 [-1,-1,-1,-1]]


# field:
# [[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true], 
#  [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false], 
#  [false,false,false,false,false,true,false,false,true,false,true,false,false,false,false,false,false,false,true,false], 
#  [false,false,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false], 
#  [false,false,false,false,false,false,false,false,true,false,false,false,false,false,false,false,false,false,false,false], 
#  ...]
# x: 8
# y: 6


# Expected Output:
# [[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  [-1,-1,-1,-1,2,1,1,2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  [-1,-1,-1,-1,1,0,0,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  ...]


field:
[[true,false,true,false,false,true], 
 [true,false,false,false,false,false], 
 [false,false,false,false,false,true], 
 [false,false,false,false,true,false]]
x: 3
y: 1


Expected Output:
[[-1,-1,-1,-1,-1,-1], 
 [-1,3,1,1,-1,-1], 
 [1,1,0,1,-1,-1], 
 [0,0,0,1,-1,-1]]



field:
[[true,false,false,false,true,false], 
 [false,false,false,false,false,false], 
 [false,false,false,false,false,false], 
 [false,false,false,false,false,false], 
 [true,false,false,false,false,true], 
 [true,false,false,false,false,true], 
 [true,true,true,false,false,false]]
x: 2
y: 2

Expected Output:
[[-1,1,0,1,-1,-1], 
 [1,1,0,1,1,1], 
 [0,0,0,0,0,0], 
 [1,1,0,0,1,1], 
 [-1,2,0,0,2,-1], 
 [-1,5,2,1,2,-1], 
 [-1,-1,-1,-1,-1,-1]]

# field:
# [[true,false,true,false,false,false,false,false,false,false], 
#  [false,false,false,false,false,false,false,false,false,false], 
#  [false,false,false,false,true,false,false,false,false,false], 
#  [false,false,false,true,false,false,false,false,false,true], 
#  [true,false,false,false,false,false,false,true,false,false], 
#  [true,false,false,false,false,false,true,false,false,false], 
#  [false,false,false,false,false,false,true,false,false,false], 
#  [false,true,false,false,false,false,false,false,false,false], 
#  [false,false,false,false,false,false,false,false,false,false], 
#  [false,true,false,false,false,true,false,false,false,false], 
#  ...]
# x: 8
# y: 9

# Expected Output:
# [[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], 
#  [-1,2,1,1,1,1,-1,-1,2,1], 
#  [-1,2,0,0,0,2,-1,3,1,0], 
#  [-1,2,1,0,0,2,-1,2,0,0], 
#  [-1,-1,1,0,0,1,1,1,0,0], 
#  [-1,-1,2,0,1,1,1,0,0,0], 
#  [-1,-1,1,0,1,-1,1,0,0,0], 
#  ...]