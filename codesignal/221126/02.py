def solution(s):
    count = 0
    for i in range(1,len(s)-1):
        for j in range(1,len(s) - i):
            k = len(s) - i - j
            # print(i, j,k)
            first = s[:i]
            mid = s[i:(i+j)]
            last = s[(i+j):]
            # print(first, mid, last)
            if first + mid != mid + last and mid + last != last + first and first + mid != last + first:
                count += 1
                # print('count')
    return count


s: "xzxzx"
Expected Output:
5

s: "xzy"
Expected Output:
1

s: "xxx"
Expected Output:
0

s: "xzxzxzxzxz"
Expected Output:
30

s: "xxxxxxxxxx"
Expected Output:
24

s: "xyzxyzxyzx"
Expected Output:
35

s: "xzxzxxzzxx"
Expected Output:
36

s: "gggggggggggggggggggggggggggggg"
Expected Output:
366

s: "gfgfgfgfgfgfgfgfgfgfgfgfgfgfgf"
Expected Output:
387