def solution(a):
    print(a)
    l, r = 0, len(a)-1
    b = []
    while l <= r:
        last = 0
        if l < r:
            if a[l] < a[r] and a[l] != last:
                print(a[l], a[r])
                b.append(a[l])
                b.append(a[r])
            else:
                return False
        else:
            b.append(a[l])
        last = a[r]
        l += 1
        r -= 1
    b_copy = b[:]
    b_copy.sort()
    print(b, "\n" ,b_copy)
    return b == b_copy


a = [1, 3, 5, 6, 4, 2]
# solution(a) = true
solution(a)

a = [1, 4, 5, 6, 3]
# solution(a) = false
