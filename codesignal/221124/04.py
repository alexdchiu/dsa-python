import math

# for i and j where i <= j
# how many pairs have sum which is exponent of 2?

def solution(numbers):
    copy = numbers[:]
    res = 0
    copy.sort(reverse=True)
    max_sum = copy[0] + copy[1]
    check = {}
    max_power_of_two = int(math.log(max_sum, 2))
    # print(numbers, copy)
    # print(max_power_of_two)
    for k in range(max_power_of_two+1):
        curr = 2**k
        check[curr] = True
    for i in range(len(numbers)):
        for j in range(i, len(numbers)):
            # print(i, j, numbers[i], numbers[j])
            print(i, j)
            curr_sum = numbers[i] + numbers[j]
            print(curr_sum)
            if curr_sum in check:
                res += 1
                print('add')
    return res

numbers = [1, -1, 2, 3]
solution(numbers) = 5


numbers = [2]
solution(numbers) = 1

numbers = [-2, -1, 0, 1, 2]
solution(numbers) = 5