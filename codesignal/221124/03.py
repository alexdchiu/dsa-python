def solution(s1, s2):
    d1, d2 = {}, {}
    for char in s1:
        if char not in d1:
            d1[char] = 0
        d1[char] += 1
    for char in s2:
        if char not in d2:
            d2[char] = 0
        d2[char] += 1
    point1, point2 = 0, 0
    res = []
    print(d1, d2)
    while point1 < len(s1) and point2 < len(s2):
        k1 = s1[point1]
        k2 = s2[point2]
        if d1[k1] < d2[k2]:
            res.append(k1)
            # print('k1', res, k1)
            point1 += 1
        if d2[k2] < d1[k1]:
            res.append(k2)
            # print('k2', res, k2)
            point2 += 1
        if d1[k1] == d2[k2]:
            if k1 <= k2:
                res.append(k1)
                point1 += 1
            else:
                res.append(k2)
                point2 += 1
    # print(res, point1, point2)
    if point1 < len(s1):
        res.append(s1[point1:])
    if point2 < len(s2):
        res.append(s2[point2:])
    # print(res)
    return ''.join(res)


s1 = "dce"
s2 = "cccbd"
solution(s1, s2) = "dcecccbd"

s1 = "super"
s2 = "tower"
solution(s1, s2) = "stouperwer"