def solution(numbers):
    l, r = 0, 0
    res = []
    while r < len(numbers):
        if r - l == 2:
            if (numbers[l] < numbers[l+1] and numbers[r-1] > numbers[r]) or (numbers[l] > numbers[l+1] and numbers[r-1] < numbers[r]):
              res.append(1)
            else:
              res.append(0)
        r += 1
        if r - l > 2:
            l += 1
    print(res)

numbers = [1, 2, 1, 3, 4]
solution(numbers)
# solution(numbers) = [1, 1, 0]

numbers = [1, 2, 3, 4]
solution(numbers)
# solution(numbers) = [0, 0]

numbers = [1000000000, 1000000000, 1000000000]
solution(numbers)
# solution(numbers) = [0]