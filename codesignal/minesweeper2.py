from collections import deque

# def get_neighbors(row: int, col: int, m: int, n: int):
#     res = []
#     for ro, co in [
#         (-1, -1),
#         (0, -1),
#         (1, -1),
#         (-1, 0),
#         (1, 1),
#         (1, 0),
#         (-1, 1),
#         (0, 1),
#     ]:
#         nrow = row + ro
#         ncol = col + co
#         if 0 <= nrow < m and 0 <= ncol < n:
#             res.append((nrow, ncol))
#     return res

# def solution(grid, x, y):
#     res = [[-1 for _ in range(len(grid[0]))] for _ in range(len(grid))]
#     queue = deque([(x, y)])
#     while queue:
#         row, col = queue.pop()
#         mine_count = 0
#         for nrow, ncol in get_neighbors(row, col, len(grid), len(grid[0])):
#             if grid[nrow][ncol]:
#                 mine_count += 1
#         if mine_count > 0:
#             res[row][col] = mine_count
#         else:
#             res[row][col] = 0
#             for nrow, ncol in get_neighbors(row, col, len(grid), len(grid[0])):
#                 if not grid[nrow][ncol] and res[nrow][ncol] == -1:
#                     queue.append((nrow, ncol))
#     return res


def solution(grid, x, y):
    res = [[-1 for _ in range(len(grid[0]))] for _ in range(len(grid))]
    queue = deque([(x, y)])
    while queue:
        row, col = queue.popleft()
        mine_count = 0
        for nrow, ncol in get_neighbors(row, col, len(grid), len(grid[0])):
            if grid[nrow][ncol]:
                mine_count += 1
        if mine_count > 0:
            res[row][col] = mine_count
        else:
            res[row][col] = 0
            for nrow, ncol in get_neighbors(row, col, len(grid), len(grid[0])):
                if not grid[nrow][ncol] and res[nrow][ncol] == -1:
                    queue.append((nrow, ncol))
    # print(res)
    return res


def get_neighbors(x, y, m, n):
    res = []
    for r, c in [
        (-1,-1),
        (-1,0),
        (-1,1),
        (0,-1),
        (0,1),
        (1,-1),
        (1,0),
        (1,1)
    ]:
        nrow, ncol = x + r, y + c
        if 0 <= nrow < m and 0 <= ncol < n:
            res.append((nrow, ncol))
    return res


tests = [
    (
        [
            [False,True,True],
            [True,False,True],
            [False,False,True]
        ],
        1,
        1,
        [
            [-1,-1,-1],
            [-1,5,-1],
            [-1,-1,-1]
        ]
    ),
    (
        [
            [True,False,True,True,False],
            [True,False,False,False,False],
            [False,False,False,False,False],
            [True,False,False,False,False]
        ],
        3,
        2,
        [[-1,-1,-1,-1,-1],
        [-1,3,2,2,1],
        [-1,2,0,0,0],
        [-1,1,0,0,0]]
    ),
    (
        [[False,True],
        [False,False]],
        0,
        0,
        [[1,-1],
        [-1,-1]]
    ),
    (
        [[True,False,False],
        [False,True,False]],
        0,
        1,
        [[-1,2,-1],
        [-1,-1,-1]]
    ),
    (
        [[False,False,False],
        [False,False,False],
        [False,False,False],
        [False,True,True],
        [False,False,False]],
        1,
        0,
        [[0,0,0],
        [0,0,0],
        [1,2,2],
        [-1,-1,-1],
        [-1,-1,-1]]
    ),
    (
        [[False,False,False,False,False,False],
        [False,False,False,False,False,False],
        [False,False,False,False,False,False],
        [False,False,False,False,False,False]],
        2,
        5,
        [[0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0]]
    ),
    (
        [[False,False,True],
        [True,False,True],
        [False,True,False],
        [True,False,False],
        [False,True,False],
        [False,True,False]],
        0,
        0,
        [[1,-1,-1],
        [-1,-1,-1],
        [-1,-1,-1],
        [-1,-1,-1],
        [-1,-1,-1],
        [-1,-1,-1]]
    ),
    (
        [[False,False,True,False],
        [False,False,False,False],
        [True,False,False,False],
        [False,True,False,False],
        [False,False,True,False],
        [False,False,False,False],
        [False,False,False,False],
        [False,False,False,False]],
        3,
        0,
        [[-1,-1,-1,-1],
        [-1,-1,-1,-1],
        [-1,-1,-1,-1],
        [2,-1,-1,-1],
        [-1,-1,-1,-1],
        [-1,-1,-1,-1],
        [-1,-1,-1,-1],
        [-1,-1,-1,-1]]
    )
]

for grid, x, y, expected in tests:
    assert solution(grid, x, y) == expected, f"grid: {grid} x: {x} y: {y} expected: {expected}"
print("All passed!")