class Node:
  def __init__(self, value):
    self.value = value
    self.next = None

class LinkedList:
  def __init__(self, value):
    # create a new node / initialize a list
    new_node = Node(value)
    self.head = new_node
    self.tail = new_node
    self.length = 1
  
  def print_list(self):
    temp = self.head
    while temp:
      print(temp.value)
      temp = temp.next

  def append(self, value):
    # create new node add node to end
    new_node = Node(value)

    if self.head is None:
      self.head = new_node
      self.tail = new_node

    else:
      self.tail.next = new_node
      self.tail = new_node
    self.length += 1
    return True

  def pop(self):
    if self.length == 0:
      return None
    curr = self.head
    prev = self.head
    while curr.next:
      prev = curr
      curr = curr.next
    self.tail = prev
    self.tail.next = None
    self.length -= 1
    if self.length == 0:
      self.head = None
      self.tail = None
    return curr.value


  # def prepend(self, value):
    # create new node and add node to beginning
  
  # def insert(self, index, value):
    # create new node and insert in middle

  # def remove(self, index):

my_linked_list = LinkedList(1)
my_linked_list.append(2)
# print(my_linked_list.head.value)
# print(my_linked_list.head.next)
# print(my_linked_list.tail)
my_linked_list.print_list()
print(my_linked_list.pop())
print(my_linked_list.pop())
print(my_linked_list.pop())