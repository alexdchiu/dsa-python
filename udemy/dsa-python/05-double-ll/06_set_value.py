class Node:
  def __init__(self, value):
    self.value = value
    self.next = None
    self.prev = None

class DoublyLinkedList:
  def __init__(self, value):
    new_node = Node(value)
    self.head = new_node
    self.tail = new_node
    self.length = 1

  def print_list(self):
    temp = self.head
    while temp:
      print(temp.value)
      temp = temp.next

  def append(self, value):
    new_node = Node(value)
    if self.length == 0:
    # if self.head == None:
      self.head = new_node
      self.tail = new_node
    else:
      self.tail.next = new_node
      new_node.prev = self.tail
      self.tail = new_node
    self.length += 1
    return True
  
  def pop(self):
    if self.length == 0:
      return None
    temp = self.tail
    if self.length == 1:
      self.head = None
      self.tail = None
    else:
      self.tail = temp.prev
      self.tail.next = None
      temp.prev = None
    self.length -= 1
    return temp
  
  def prepend(self, value):
    new_node = Node(value)
    if self.length == 0:
      self.tail = new_node
    else:
      new_node.next = self.head
      self.head.prev = new_node
    self.head = new_node
    self.length += 1
    return True
  
  def pop_first(self):
    if self.length == 0:
      return None
    temp = self.head
    if self.length == 1:
      self.head = None
      self.tail = None
    else:
      self.head = temp.next
      self.head.prev = None
      temp.next = None
    self.length -= 1
    return temp
  
  def get(self, index):
    if index < 0 or index >= self.length:
      return None
    if index < self.length / 2:
      temp = self.head
      for _ in range(index):
        temp = temp.next
    else:
      temp = self.tail
      # for _ in range(self.length - 1 - index):
      for _ in range(self.length-1, index, -1):
        temp = temp.prev
    return temp
  
  def set_value(self, index, value):
    if index < 0 or index >= self.length:
      return False
    temp = self.get(index)
    temp.value = value
    return True
  
  def insert(self, index, value):
    if index < 0 or index > self.length:
      return False
    if index == self.length:
      self.append(value)
    if index == 0:
      self.prepend(value)
    
    new_node = Node(value)
    temp = self.get(index)
    prev = temp.prev
    new_node.prev = prev
    prev.next = new_node
    new_node.next = temp
    temp.prev = new_node

    # prev = self.get(index - 1)
    # nxt = prev.next
    # prev.next = new_node
    # new_node.prev = prev
    # nxt.prev = new_node
    # new_node.next = nxt

    self.length += 1
    return True

my_dll = DoublyLinkedList(1)
my_dll.append(3)

my_dll.insert(1,2)

my_dll.print_list()
