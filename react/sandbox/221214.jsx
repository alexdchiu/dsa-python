// https://levelup.gitconnected.com/learn-hooks-in-just-5-steps-with-examples-44436e825f01
// https://codesandbox.io/s/todo-with-hooks-initial-ui-forked-m1vpew?file=/src/index.js


import React, {useState, useEffect} from "react";
import localforage from "localforage"
import ReactDOM from "react-dom";


import "./styles.css";

function App() {
  const [todos, setTodos] = useState([
      {name: 'do pushups', status: true, id:1},
      {name: 'do situps', status: false, id:2}
  ])
  const [formData, setFormData] = useState("")
  const onFormDataChange = ({target: {value}}) => {
    setFormData(value)
  }

  const addTodo = () => {
    if (formData !== "") {
      setTodos (
        [
          ...todos, 
          { name:formData, status: false, id: Date.now() + Math.random() }
        ])
      setFormData('')
    }
  }

  const deleteTodo = (id) => {
    setTodos(todos.filter(todo => todo.id !== id))
  }

  const handleCheckboxChange = id => {
    setTodos(
      todos.map(
        todo => {
          if (todo.id === id) return {
            ...todo, status: !todo.status
          }
          return todo
        }
      )
    )
  }

  const handleKeyPress = ({key}) => {
    if (key === "Enter") {
      addTodo()
    }
  }


  useEffect(() => {
    localforage.setItem('todos', todos);
  },
    [todos]
  );

  return (
    <div className="container">
      <p>
        <label>Add Item</label>
        <input id="new-task" type="text" value={formData} name="todoField" onChange={onFormDataChange} onKeyDown={handleKeyPress}/>
        <button onClick={addTodo}>Add</button>
      </p>

      <h3>Todo</h3>
      <ul id="incomplete-tasks">
        {todos
          .filter(todo => !todo.status)
          .map((todo, i) => (
            <li key={todo.id}>
              <input type="checkbox" checked={todo.status} onChange={() => handleCheckboxChange(todo.id)}/>
              <label>{todo.name}</label>
              <button className="delete" onClick={() => deleteTodo(todo.id)}>Delete</button>
            </li>
          ))}
      </ul>

      <h3>Completed</h3>
      <ul id="completed-tasks">
        {todos
          .filter(todo => todo.status)
          .map((todo,i) => (
            <li>
              <input type="checkbox" checked={todo.status} onChange={() => handleCheckboxChange(todo.id)}/>
              <label>{todo.name}</label>
              <button className="delete" onClick={() => deleteTodo(todo.id)}>Delete</button>
            </li>
          ))
        }
      </ul>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
