def count_closed_islands_dfs(matrix):
  rows = len(matrix)
  cols = len(matrix[0])
  visited = [[False for i in range(cols)] for j in range(rows)]
  islands = 0

  for i in range(rows):
    for j in range(cols):
      if matrix[i][j] == 1 and not visited[i][j]:
        if is_closed_island(matrix, visited, i, j):
          islands += 1

  return islands

def is_closed_island(matrix, visited, x, y):
  if x < 0 or x >= len(matrix) or y < 0 or y >= len(matrix[0]):
    return False
  
  if matrix[x][y] == 0 or visited[x][y]:
    return True

  visited[x][y] = True

  is_closed = True

  is_closed &= is_closed_island(matrix, visited, x+1, y)
  is_closed &= is_closed_island(matrix, visited, x-1, y)
  is_closed &= is_closed_island(matrix, visited, x, y+1)
  is_closed &= is_closed_island(matrix, visited, x, y-1)

  return is_closed

def main():
    print(count_closed_islands_dfs([[1, 1, 0, 0, 0], [0, 1, 0, 0, 0], [
          0, 0, 1, 1, 0], [0, 1, 1, 0, 0], [0, 0, 0, 0, 0]]))

    print(count_closed_islands_dfs([[0, 0, 0, 0], [0, 1, 0, 0], [
          0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0]]))


main()