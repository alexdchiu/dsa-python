def find_island_perimeter(matrix):
  rows = len(matrix)
  cols = len(matrix[0])
  visited = [[False for i in range(cols)] for j in range(rows)]

  for i in range(rows):
    for j in range(cols):
      if matrix[i][j] == 1 and not visited[i][j]:
        return island_perimeter_dfs(matrix, visited, i, j)

  return 0

def island_perimeter_dfs(matrix, visited, x, y):
  if x < 0 or x >= len(matrix) or y < 0 or y >= len(matrix[0]):
    return 1

  if matrix[x][y] == 0:
    return 1

  if visited[x][y]:
    return 0
  
  visited[x][y] = True

  count = 0
  count += island_perimeter_dfs(matrix, visited, x+1, y)
  count += island_perimeter_dfs(matrix, visited, x-1, y)
  count += island_perimeter_dfs(matrix, visited, x, y+1)
  count += island_perimeter_dfs(matrix, visited, x, y-1)
  
  return count




def main():
    print(find_island_perimeter([[1, 1, 0, 0, 0],
                               [0, 1, 0, 0, 0],
                               [0, 1, 0, 0, 0],
                               [0, 1, 1, 0, 0],
                               [0, 0, 0, 0, 0]]))

    print(find_island_perimeter([[0, 0, 0, 0],
                               [0, 1, 0, 0],
                               [0, 1, 0, 0],
                               [0, 1, 1, 0],
                               [0, 1, 0, 0]]))


main()