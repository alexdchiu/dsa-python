def max_area_dfs(matrix):
  rows = len(matrix)
  cols = len(matrix[0])
  max_area = 0

  for i in range(rows):
    for j in range(cols):
      if matrix[i][j] == 1:
        max_area = max(
          max_area,
          visit_island_dfs(matrix, i, j)
        )

  return max_area


def visit_island_dfs(matrix, x, y):
  if x < 0 or x >= len(matrix) or y < 0 or y >= len(matrix[0]):
    return 0
  
  if matrix[x][y] == 0:
    return 0

  matrix[x][y] = 0

  area = 1
  area += visit_island_dfs(matrix, x+1, y)
  area += visit_island_dfs(matrix, x-1, y)
  area += visit_island_dfs(matrix, x, y+1)
  area += visit_island_dfs(matrix, x, y-1)
  return area

def main():
    print(max_area_dfs([[1, 1, 1, 0, 0], [0, 1, 0, 0, 1], [
          0, 0, 1, 1, 0], [0, 1, 1, 0, 0], [0, 0, 1, 0, 0]]))


main()