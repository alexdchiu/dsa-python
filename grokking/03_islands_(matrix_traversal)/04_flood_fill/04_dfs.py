def flood_fill(matrix, x, y, new_color):
  if matrix[x][y] != new_color:
    fill_dfs(matrix, x, y, matrix[x][y], new_color)
  return matrix

def fill_dfs(matrix, x, y, old_color, new_color):
  if x < 0 or x >= len(matrix) or y < 0 or y >= len(matrix[0]):
    return
  
  if matrix[x][y] != old_color:
    return

  matrix[x][y] = new_color

  fill_dfs(matrix, x+1, y, old_color, new_color)
  fill_dfs(matrix, x-1, y, old_color, new_color)
  fill_dfs(matrix, x, y+1, old_color, new_color)
  fill_dfs(matrix, x, y-1, old_color, new_color)


def main():
    print(flood_fill([[0, 1, 1, 1, 0], [0, 0, 0, 1, 1], [
          0, 1, 1, 1, 0], [0, 1, 1, 0, 0], [0, 0, 0, 0, 0]], 1, 3, 2))
    print(flood_fill([[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [
          0, 0, 1, 1, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]], 3, 2, 5))


main()
