from collections import deque

def count_islands_bfs(matrix):
  rows = len(matrix)
  cols = len(matrix[0])
  island_count = 0

  for i in range(rows):
    for j in range(cols):
      if matrix[i][j] == 1:
        island_count += 1
        visit_island_bfs(matrix, i, j)
  
  return island_count


def visit_island_bfs(matrix, x, y):
  neighbors = deque([(x,y)])
  while neighbors:
    row, col = neighbors.popleft()

    if row < 0 or row >= len(matrix) or col < 0 or col >= len(matrix[0]):
      continue
    if matrix[row][col] == 0:
      continue
    matrix[row][col] = 0

    neighbors.extend([(row+1, col)])
    neighbors.extend([(row-1, col)])
    neighbors.extend([(row, col+1)])
    neighbors.extend([(row, col-1)])


def main():
    print(count_islands_bfs([[0, 1, 1, 1, 0], [0, 0, 0, 1, 1], [
          0, 1, 1, 1, 0], [0, 1, 1, 0, 0], [0, 0, 0, 0, 0]]))
    print(count_islands_bfs([[1, 1, 1, 0, 0], [0, 1, 0, 0, 1], [
          0, 0, 1, 1, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]]))


main()