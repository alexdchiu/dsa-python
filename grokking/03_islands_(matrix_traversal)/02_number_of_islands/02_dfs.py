def count_islands_dfs(matrix):
  island_count = 0
  rows = len(matrix)
  cols = len(matrix[0])

  for i in range(rows):
    for j in range(cols):
      if matrix[i][j] == 1:
        island_count += 1
        visit_island_dfs(matrix, i, j)
  
  return island_count


def visit_island_dfs(matrix, x, y):
  if x < 0 or x >= len(matrix) or y < 0 or y >= len(matrix[0]):
    return
  if matrix[x][y] == 0:
    return
  
  matrix[x][y] = 0

  visit_island_dfs(matrix, x+1, y)
  visit_island_dfs(matrix, x-1, y)
  visit_island_dfs(matrix, x, y+1)
  visit_island_dfs(matrix, x, y-1)

def main():
    print(count_islands_dfs([[0, 1, 1, 1, 0], [0, 0, 0, 1, 1], [
          0, 1, 1, 1, 0], [0, 1, 1, 0, 0], [0, 0, 0, 0, 0]]))
    print(count_islands_dfs([[1, 1, 1, 0, 0], [0, 1, 0, 0, 1], [
          0, 0, 1, 1, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]]))


main()