def separate_duplicates(arr):
  i, next = 0, 1
  while i < len(arr):
    if arr[next - 1] != arr[i]:
      arr[next] = arr[i]
      next += 1
    i += 1
  return next

def main():
  print(separate_duplicates([2, 3, 3, 3, 6, 9, 9]))
  print(separate_duplicates([2, 2, 2, 11]))


main()