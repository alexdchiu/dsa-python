def pair_with_targetsum(arr, target):
  l, r = 0, len(arr) - 1

  while l < r:
    curr = arr[l] + arr[r]
    if curr == target:
      return [l, r]
    
    if target > curr:
      l += 1
    else:
      r -= 1
  
  return [-1, -1]


def main():
  print(pair_with_targetsum([1, 2, 3, 4, 6], 6))
  print(pair_with_targetsum([2, 5, 9, 11], 11))

main()