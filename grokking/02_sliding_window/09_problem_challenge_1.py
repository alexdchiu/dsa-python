from curses import window
from turtle import left, right


def find_permutation(str1, pattern):
  # window_start, matched = 0, 0
  # char_obj = {}

  # for c in pattern:
  #   if c not in char_obj:
  #     char_obj[c] = 0
  #   char_obj[c] += 1
  
  # for window_end in range(len(str1)):
  #   right_char = str1[window_end]
  #   if right_char in char_obj:
  #     char_obj[right_char] -= 1
  #     if char_obj[right_char] == 0:
  #       matched += 1
    
  #   if len(char_obj) == matched:
  #     return True
    
  #   if window_end >= len(pattern) - 1:
  #     left_char = str1[window_start]
  #     if left_char in char_obj:
  #       if char_obj[left_char] == 0:
  #         matched -= 1
  #       char_obj[left_char] += 1
  #     window_start += 1
    
  # return False


def main():
    print('Permutation exist: ' + str(find_permutation("oidbcaf", "abc")))
    print('Permutation exist: ' + str(find_permutation("odicf", "dc")))
    print('Permutation exist: ' + str(find_permutation("bcdxabcdy", "bcdyabcdx")))
    print('Permutation exist: ' + str(find_permutation("aaacb", "abc")))


main()