from turtle import right


def fruits_into_baskets(fruits):
  window_start = 0
  max_length = 0
  fruit_obj = {}

  for window_end in range(len(fruits)):
    right_fruit = fruits[window_end]
    if right_fruit not in fruit_obj:
      fruit_obj[right_fruit] = 0
    fruit_obj[right_fruit] += 1

    while len(fruit_obj) > 2:
      left_fruit = fruits[window_start]
      fruit_obj[left_fruit] -= 1
      if fruit_obj[left_fruit] == 0:
        del fruit_obj[left_fruit]
      window_start += 1

    max_length = max(max_length, window_end - window_start + 1)

  return max_length

def main():
    print("Maximum number of fruits: "
          + str(fruits_into_baskets(['A', 'B', 'C', 'A', 'C'])))
    print("Maximum number of fruits: "
          + str(fruits_into_baskets(['A', 'B', 'C', 'B', 'B', 'C'])))


main()