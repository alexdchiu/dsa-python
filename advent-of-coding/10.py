from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=10)

input = lines(puzzle.input_data)

# input = [
#   "addx 15",
#   "addx -11",
#   "addx 6",
#   "addx -3",
#   "addx 5",
#   "addx -1",
#   "addx -8",
#   "addx 13",
#   "addx 4",
#   "noop",
#   "addx -1",
#   "addx 5",
#   "addx -1",
#   "addx 5",
#   "addx -1",
#   "addx 5",
#   "addx -1",
#   "addx 5",
#   "addx -1",
#   "addx -35",
#   "addx 1",
#   "addx 24",
#   "addx -19",
#   "addx 1",
#   "addx 16",
#   "addx -11",
#   "noop",
#   "noop",
#   "addx 21",
#   "addx -15",
#   "noop",
#   "noop",
#   "addx -3",
#   "addx 9",
#   "addx 1",
#   "addx -3",
#   "addx 8",
#   'addx 1',
#   "addx 5",
#   "noop",
#   "noop",
#   "noop",
#   "noop",
#   "noop",
#   "addx -36",
#   "noop",
#   "addx 1",
#   "addx 7",
#   "noop",
#   "noop",
#   "noop",
#   "addx 2",
#   "addx 6",
#   "noop",
#   "noop",
#   "noop",
#   "noop",
#   "noop",
#   "addx 1",
#   "noop",
#   "noop",
#   "addx 7",
#   "addx 1",
#   "noop",
#   "addx -13",
#   "addx 13",
#   "addx 7",
#   "noop",
#   "addx 1",
#   "addx -33",
#   "noop",
#   "noop",
#   "noop",
#   "addx 2",
#   "noop",
#   "noop",
#   "noop",
#   "addx 8",
#   "noop",
#   "addx -1",
#   "addx 2",
#   "addx 1",
#   "noop",
#   "addx 17",
#   "addx -9",
#   "addx 1",
#   "addx 1",
#   "addx -3",
#   "addx 11",
#   "noop",
#   "noop",
#   "addx 1",
#   "noop",
#   "addx 1",
#   "noop",
#   "noop",
#   "addx -13",
#   "addx -19",
#   "addx 1",
#   "addx 3",
#   "addx 26",
#   "addx -30",
#   "addx 12",
#   "addx -1",
#   "addx 3",
#   "addx 1",
#   "noop",
#   "noop",
#   "noop",
#   "addx -9",
#   "addx 18",
#   "addx 1",
#   "addx 2",
#   "noop",
#   "noop",
#   "addx 9",
#   "noop",
#   "noop",
#   "noop",
#   "addx -1",
#   "addx 2",
#   "addx -37",
#   "addx 1",
#   "addx 3",
#   "noop",
#   "addx 15",
#   "addx -21",
#   "addx 22",
#   'addx -6',
#   'addx 1',
#   "noop",
#   'addx 2',
#   'addx 1',
#   "noop",
#   'addx -10',
#   "noop",
#   "noop",
#   'addx 20',
#   'addx 1',
#   'addx 2',
#   'addx 2',
#   'addx -6',
#   'addx -11',
#   "noop",
#   "noop",
#   "noop",
# ]


################# solution 1
curr_cycle = 0

cycle_track = {}

x = 1
for line in input:
  if line.split()[0] == 'noop':
    curr_cycle += 1
    cycle_track[curr_cycle] = x
  else:
    curr_cycle += 1
    cycle_track[curr_cycle] = x
    curr_cycle += 1
    cycle_track[curr_cycle] = x
    x += int(line.split()[1])

# print(cycle_track)

target = [20,60,100,140,180,220]
res = []
for t in target:
  res.append(cycle_track[t] * t)

print(sum(res))


################# solution 2
def solution2(input):
  crt = 0
  res = []
  curr_row = []
  row_length = 40
  sprite_start = 0

  for line in input:
    in_sprite = sprite_start <= crt <= sprite_start + 2
    
    #if noop
    if line.split()[0] == 'noop':
      curr_row, crt, res = pixel(crt, in_sprite, curr_row, row_length, res)
    
    #if addx
    else:
      curr_row, crt, res = pixel(crt, in_sprite, curr_row, row_length, res)
      
      # need to redclare if in_sprite
      in_sprite = sprite_start <= crt <= sprite_start + 2
      curr_row, crt, res = pixel(crt, in_sprite, curr_row, row_length, res)
      
      # move sprite at end of second cycle
      sprite_start += int(line.split()[1])
      
  # print(res)
  for item in res:
    print(item)

def pixel(crt, in_sprite, curr_row, row_length, res):
  crt += 1
  crt = crt % row_length
  if in_sprite:
    curr_row.append("#")
  else:
    curr_row.append(".")
  if len(curr_row) == row_length:
    res.append(''.join(curr_row))
    curr_row = []
  return curr_row, crt, res

solution2(input)