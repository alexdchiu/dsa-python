from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=8)

input = lines(puzzle.input_data)

# grid = [
#   [3,0,3,7,3],
#   [2,5,5,1,2],
#   [6,5,3,3,2],
#   [3,3,5,4,9],
#   [3,5,3,9,0]
# ]

grid = []
for s in input:
  curr = []
  for digit in s:
    curr.append(int(digit))
  grid.append(curr)

# print(grid)

# count = 0
# for r in range(len(grid)):
#   start = grid[r][0]
#   curr_tallest = start
#   count += 1
#   # print('start', start, count)
#   for c in range(1,len(grid[0])):
#     curr = grid[r][c]
#     if curr > curr_tallest:
#       count += 1
#       # print(curr, count)
#       curr_tallest = curr

def solution1(grid):
  count = len(grid) * 2
  count += (len(grid[0])-2) * 2
  for r in range(1,len(grid)-1):
    for c in range(1,len(grid[0])-1):
      curr = (r,c)
      count += is_visible(grid,curr,len(grid), len(grid[0]))
  print(count)


def is_visible(grid,curr,m,n):
  # m = len(grid)
  # n = len(grid[0])
  r,c = curr
  curr_tree = grid[r][c]
  #go up
  while r > 0:
    r -= 1
    if grid[r][c] >= curr_tree:
      break
    if r == 0:
      return 1
  
  #go down
  r,c = curr
  while r < m-1:
    r += 1
    if grid[r][c] >= curr_tree:
      break
    if r == m-1:
      return 1
  
  #go left
  r,c = curr
  while c > 0:
    c -= 1
    if grid[r][c] >= curr_tree:
      break
    if c == 0:
      return 1
  
  #go right
  r,c = curr
  while c < n-1:
    c += 1
    if grid[r][c] >= curr_tree:
      break
    if c == n-1:
      return 1
  
  return 0



def solution2(grid):
  res = 0
  for r in range(1,len(grid)-1):
    for c in range(1,len(grid[0])-1):
      res = max(res,scenic_score(grid,(r,c),len(grid),len(grid[0])))
  print(res)

def scenic_score(grid,curr,m,n):
  # m = len(grid)
  # n = len(grid[0])
  r,c = curr
  curr_tree = grid[r][c]
  
  #go up
  up = 0
  while r > 0:
    r -= 1
    up += 1
    if grid[r][c] >= curr_tree:
      break
  #go down
  r,c = curr
  down = 0
  while r < m-1:
    r += 1
    down += 1
    if grid[r][c] >= curr_tree:
      break
  
  #go left
  r,c = curr
  left = 0
  while c > 0:
    c -= 1
    left += 1
    if grid[r][c] >= curr_tree:
      break
  
  #go right
  r,c = curr
  right = 0
  while c < n-1:
    c += 1
    right += 1
    if grid[r][c] >= curr_tree:
      break
  
  return up * down * left * right

# solution1(grid)
solution2(grid)