from aocd.models import Puzzle
from aocd.transforms import lines

from collections import deque

puzzle = Puzzle(year=2022, day=13)

input = lines(puzzle.input_data)

for i in range(0,9,3):
  lst1 = []
  lst2 = []

  for j in range(len(input[i])):
    if input[i][j].isdigit():
      lst1.append(int(input[i][j]))

  for k in range(len(input[i+1])):
    if input[i][k].isdigit():
      lst2.append(int(input[i][k]))
  
  print(input[i])
  print(input[i+1])
  print(input[i+2])

  print(lst1)
  print(lst2)