from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=6)

input = lines(puzzle.input_data)
datastream = input[0]

r = 0
l = 0
tgt_length = 14
track = {}
curr_count = 0
while r < len(datastream):
# while r < 20:
  r_char = datastream[r]
  l_char = datastream[l]

  print(r, r_char, l, l_char)

  if r_char not in track:
    track[r_char] = 0
    curr_count += 1
  track[r_char] += 1


  if r - l > tgt_length-1:
    track[l_char] -= 1
    if track[l_char] == 0:
      curr_count -= 1
      del track[l_char]
    l += 1

  r += 1

  if curr_count == tgt_length:
    print('ANSWER -', r, track)  
    break
