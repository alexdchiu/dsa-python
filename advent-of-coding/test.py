from aocd.models import Puzzle
from aocd.transforms import lines

puzzle = Puzzle(year=2022, day=2)

calories = lines(puzzle.input_data)

for calorie in calories:
    print(calorie)