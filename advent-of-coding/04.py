import re

from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=4)

assignments = lines(puzzle.input_data)

pairs = []
res = 0
for assignment in assignments:
  split_str = re.split(r'[-,]',assignment)
  first_lower = int(split_str[0])
  first_upper = int(split_str[1])
  second_lower = int(split_str[2])
  second_upper = int(split_str[3])
  # second_in_first = first_lower <= second_lower <= second_upper <= first_upper
  # first_in_second = second_lower <= first_lower <= first_upper <= second_upper
  # if second_in_first or first_in_second:
  #   res += 1

  overlap1 = first_lower <= second_lower <= first_upper
  overlap2 = first_lower <= second_upper <= first_upper
  overlap3 = second_lower <= first_lower <= second_upper
  overlap4 = second_lower <= first_upper <= second_upper

  if overlap1 or overlap2 or overlap3 or overlap4:
    res += 1


print(res)
