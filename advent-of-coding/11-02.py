from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=11)

input = lines(puzzle.input_data)

for line in input:
  print(line)

from collections import deque

monkey_items = [
  deque([75, 75, 98, 97, 79, 97, 64]),
  deque([50, 99, 80, 84, 65, 95]),
  deque([96, 74, 68, 96, 56, 71, 75, 53]),
  deque([83, 96, 86, 58, 92]),
  deque([99]),
  deque([60, 54, 83]),
  deque([77, 67]),
  deque([95, 65, 58, 76]),
]

def operation_0(x):
  return x * 13

def operation_1(x):
  return x + 2

def operation_2(x):
  return x + 1

def operation_3(x):
  return x + 8

def operation_4(x):
  return x * x

def operation_5(x):
  return x + 4

def operation_6(x):
  return x * 17

def operation_7(x):
  return x + 5


operation = {
  0: operation_0,
  1: operation_1,
  2: operation_2,
  3: operation_3,
  4: operation_4,
  5: operation_5,
  6: operation_6,
  7: operation_7,
}


def test_0(x):
  if x % 19 == 0:
    return 2
  else:
    return 7

def test_1(x):
  if x % 3 == 0:
    return 4
  else:
    return 5

def test_2(x):
  if x % 11 == 0:
    return 7
  else:
    return 3

def test_3(x):
  if x % 17 == 0:
    return 6  
  else:
    return 1

def test_4(x):
  if x % 5 == 0:
    return 0  
  else:
    return 5


def test_5(x):
  if x % 2 == 0:
    return 2  
  else:
    return 0


def test_6(x):
  if x % 13 == 0:
    return 4  
  else:
    return 1


def test_7(x):
  if x % 7 == 0:
    return 3  
  else:
    return 6


test = {
  0: test_0,
  1: test_1,
  2: test_2,
  3: test_3,
  4: test_4,
  5: test_5,
  6: test_6,
  7: test_7,
}


count = {
  0: 0,
  1: 0,
  2: 0,
  3: 0,
  4: 0,
  5: 0,
  6: 0,
  7: 0,
}


items = [
  (75,0),
  (75,0),
  (98,0),
  (97,0),
  (79,0),
  (97,0),
  (64,0),
  (50,1),
  (99,1),
  (80,1),
  (84,1),
  (65,1),
  (95,1),
  (96,2),
  (74,2),
  (68,2),
  (96,2),
  (56,2),
  (71,2),
  (75,2),
  (53,2),
  (83,3),
  (96,3),
  (86,3),
  (58,3),
  (92,3),
  (99,4),
  (60,5),
  (54,5),
  (83,5),
  (77,6),
  (67,6),
  (95,7),
  (65,7),
  (58,7),
  (76,7),
]

test_item = (75,0)

for _ in range(1000):
  item, monkey_num = test_item
  print('item', item, 'monkey_num', monkey_num)
  count[monkey_num] += 1
  x = operation[monkey_num](item) // 1
  # print(x)
  next_monkey = test[monkey_num](x)
  test_item = (x, next_monkey)

print(count)

# largest = list(count.values())
# largest.sort(reverse=True)
# res = largest[0] * largest[1]
# print(res)