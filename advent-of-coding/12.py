from aocd.models import Puzzle
from aocd.transforms import lines

from collections import deque

puzzle = Puzzle(year=2022, day=12)

input = lines(puzzle.input_data)

# BFS -> queue

input = [
  'aabqponm',
  'abcryxxl',
  'accszExk',
  'acctuvwj',
  'abdefghi',
]



for line in input:
  print(line)

visited = set()

dirs = [
  (1,0),
  (-1,0),
  (0,1),
  (0,-1),
]


curr = ((0,0),0)
queue = deque([curr])
while queue:
  curr, steps = queue.popleft()
  if curr in visited:
    continue
  visited.add(curr)
  r, c = curr
  for dir in dirs:
    dir_r, dir_c = dir
    new_r, new_c = r + dir_r, c + dir_c
    r_inbounds = 0 <= new_r < len(input)
    c_inbounds = 0 <= new_c < len(input[0])
    new_pos = (new_r, new_c)
    if new_pos not in visited and r_inbounds and c_inbounds:
      new_letter = input[new_r][new_c]
      curr_letter = input[r][c]
      print(curr_letter, steps)
      if new_letter != "E":
        if ord(new_letter) - ord(curr_letter) <= 1:
          queue.append((new_pos, steps+1))
      else:
        if curr_letter == 'y' or curr_letter == 'z':
          print('ANSWER --- ', steps + 1)
      
      
      # if (ord(input[new_r][new_c]) - ord(input[r][c])) <= 1:
      #   queue.append((new_pos, steps+1))
      #   print(input[new_r][new_c], steps+1)
      # # if ((ord(input[r][c]) == ord('y')) or (ord(input[r][c]) == ord('z'))) and input[new_r][new_c] == "E":
      #   # print("ANSWER --- ", steps+1)
      # if input[new_r][new_c] == "E":
      #   print('HELLO')