from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=3)

rucksacks = lines(puzzle.input_data)
# print(rucksacks)

# a-z 1-26
# A-Z 27-52

# test = [
#   "p","L","P","v","t","s",
# ]

# shared_items = []
# for rucksack in rucksacks:
  
#   mid = len(rucksack) // 2
#   first_half = rucksack[:mid]
#   second_half = rucksack[mid:]
#   check = {}
#   # print(first_half, second_half)
#   for char1 in first_half:
#     if char1 not in check:
#       check[char1] = 0
#     check[char1] += 1
#   for char2 in second_half:
#     if char2 in check:
#       shared_items.append(char2)
#       # print(char2)
#       break

# res = 0
# for char in shared_items:
#   if char.islower():
#     res += ord(char) - ord('a') + 1

#   else:
#     res += ord(char) - ord('A') + 27

# print(res)



badges = []

for i in range(0,len(rucksacks),3):
  
  first = rucksacks[i]
  second = rucksacks[i+1]
  third = rucksacks[i+2]
  first_dict = {}
  second_dict = {}

  for char1 in first:
    if char1 not in first_dict:
      first_dict[char1] = 0
    first_dict[char1] += 1

  for char2 in second:
    if char2 not in second_dict:
      second_dict[char2] = 0
    second_dict[char2] += 1

  for char3 in third:
    if char3 in first_dict and char3 in second_dict:
      badges.append(char3)
      break
        

print(badges)

res = 0
for char in badges:
  if char.islower():
    res += ord(char) - ord('a') + 1

  else:
    res += ord(char) - ord('A') + 27

print(res)