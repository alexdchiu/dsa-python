# A = Rock
# B = Paper
# C = Scissors

# Y = Paper
# X = Rock
# Z = Scissors

# 1 point for Rock, 2 for Paper, 3 for scissors
# 0 if you lose, 3 if draw, 6 if you win

# first column is opponent

# from aocd import get_data
# import os

# aoc_session = os.environ.get("AOC_SESSION")
# print(aoc_session)
# puzzle = get_data(day=2, year=2022)
# print(puzzle)

# my_file = open('02.txt', 'r')
# data = my_file.readlines()

# points = {
#   "Rock": 1,
#   "Paper": 2,
#   "Scissors": 3
# }

# key = {
#   "X": "Rock",
#   "Y": "Paper",
#   "Z": "Scissors",
#   "A": "Rock",
#   "B": "Paper",
#   "C": "Scissors"
# }

# wins = {
#   "Rock": "Scissors",
#   "Paper": "Rock",
#   "Scissors": "Paper"
# }

# score = 0
# for line in data:
#   new_line = line.replace("\n", "")
#   first = new_line[0]
#   second = new_line[2]
#   # res.append(new_line)
#   score += points[key[second]]
#   if key[first] == key[second]:
#     score += 3
#   if wins[key[second]] == key[first]:
#     score += 6

# print(score)

# my_file = open('02.txt', 'r')
# data = my_file.readlines()

from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=2)

rounds = lines(puzzle.input_data)



points = {
  "Rock": 1,
  "Paper": 2,
  "Scissors": 3
}

outcome_points = {
  "X": 0,
  "Y": 3,
  "Z": 6,
}

key = {
  "A": { # rock
    "X": "Scissors",
    "Y": "Rock",
    "Z": "Paper"
  },
  "B": { #paper
    "X": "Rock",
    "Y": "Paper",
    "Z": "Scissors",
  },
  "C": { #scissors
    "X": "Paper",
    "Y": "Scissors",
    "Z": "Rock",
  }
}

score = 0
for line in rounds:
  # print(line)
  # new_line = line.replace("\n", "")
  opponent = line[0]
  outcome = line[2]
  score += outcome_points[outcome]
  my_choice = key[opponent][outcome]
  score += points[my_choice]

print(score)