from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=7)

input = lines(puzzle.input_data)

class File:
  def __init__(self, name, size):
    self.name = name
    self.size = size

class Directory:
  def __init__(self, name, parent):
    self.name = name
    self.parent = parent
    self.children = {}

  def size(self):
    size = 0
    for child in self.children.values():
      size += child.size

    return size

  def directories(self):
    