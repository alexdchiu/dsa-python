from aocd.models import Puzzle
from aocd.transforms import lines

import math

puzzle = Puzzle(year=2022, day=15)

input = lines(puzzle.input_data)

for line in input:
  print(line)