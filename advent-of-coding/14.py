from aocd.models import Puzzle
from aocd.transforms import lines

import math

puzzle = Puzzle(year=2022, day=14)

input = lines(puzzle.input_data)

rocks = []
for line in input:
  rocks.append(line.split(' -> '))


max_x, max_y = 0, 0
min_x, min_y = float('inf'), float('inf')
for rock in rocks:
  for coord in rock:
    x,y = coord.split(',')
    max_x = max(max_x, int(x))
    min_x = min(min_x, int(x))
    max_y = max(max_y, int(y))
    min_y = min(min_y, int(y))

# print(max_x, max_y)
# print(min_x, min_y)


x_floor = min_x // 5 * 5
x_ceil = math.ceil(max_x / 5) * 5
y_floor = 0
y_ceil = math.ceil(max_y / 5) * 5

grid = [["." for x in range(x_floor, x_ceil)] for y in range(y_floor, y_ceil)]

for rock in rocks:
  for coord in rock:
    x,y = coord.split(',')
    x = int(x) - x_floor
    grid[int(y)][x] = "#"


for row in grid:
  print("".join(row))

# print(y_floor, y_ceil, len(grid))
# print(x_floor, x_ceil, len(grid[0]))
