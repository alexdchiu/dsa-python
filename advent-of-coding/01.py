my_file = open('01.txt', 'r')

data = my_file.readlines()

# data = my_file.read().split()

# data = my_file.read()

# res = 0
# curr_count = 0

# for item in data:
#   if item == "\n":
#     res = max(res, curr_count)
#     curr_count = 0
#   else:
#     item.replace("\n", "")
#     curr_count += int(item)

# res = max(res, curr_count)

# print(res)


res = []
curr_count = 0

for item in data:
  if item == "\n":
    res.append(curr_count)
    curr_count = 0
  else:
    item.replace("\n", "")
    curr_count += int(item)

res.append(curr_count)
res.sort(reverse=True)

top_three = sum(res[:3])

print(res)
print(top_three)