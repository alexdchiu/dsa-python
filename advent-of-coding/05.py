import re

from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=5)

input = lines(puzzle.input_data)

len_of_original_stacks = 8
stack_nums = input[len_of_original_stacks]
original_stacks = input[:len_of_original_stacks]
stack_moves = input[len_of_original_stacks+2:]
stacks = {}

# get stack #s
for i in range(0,len(stack_nums),4):
  stack_num = stack_nums[i+1:i+2]
  stacks[stack_num] = []


# initialize original stacks
stack_num = 0
count = 0
print(stack_nums)
for j in range(len(original_stacks)-1,-1,-1):
  print(original_stacks[j])
  for k in range(0,len(original_stacks[j]),4):
    curr_item = original_stacks[j][k+1:k+2]
    # print(curr_item)
    stack_num += 1
    if stack_num > 9:
      stack_num = stack_num % 9
    if curr_item.isalpha():
      stacks[str(stack_num)].append(curr_item)
  
# print(stacks)


# # start stack moves q1
# for move in stack_moves:
#   print(move)
#   split_move = move.split()
#   quantity = split_move[1]
#   stack_from = split_move[3]
#   stack_to = split_move[5]
  
#   for z in range(int(quantity)):
#     curr = stacks[stack_from].pop()
#     stacks[stack_to].append(curr)

#   # print(stacks)

# # print(stacks)

# res = ""
# for stack_num in stacks:
#   if len(stacks[stack_num]) > 0:
#     idx = len(stacks[stack_num])-1
#     res += stacks[stack_num][idx]

# print(res)


# start stack moves q2
for move in stack_moves:
  print(move)
  split_move = move.split()
  quantity = int(split_move[1])
  stack_from = split_move[3]
  stack_to = split_move[5]

  curr_stack = stacks[stack_from]
  curr_len = len(curr_stack)
  new_end = curr_len - quantity
  new_curr_stack = curr_stack[:new_end]
  being_moved = curr_stack[new_end:]
  # print(curr_stack, curr_len, new_curr_stack, being_moved)
  stacks[stack_from] = new_curr_stack
  stacks[stack_to] += being_moved

# print(stacks)

res = ""
for stack_num in stacks:
  if len(stacks[stack_num]) > 0:
    idx = len(stacks[stack_num])-1
    res += stacks[stack_num][idx]

print(res)