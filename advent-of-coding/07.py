from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=7)

input = lines(puzzle.input_data)

input = [
  "$ cd /",
  "$ ls",
  "dir a",
  "14848514 b.txt",
  "8504156 c.dat",
  "dir d",
  "$ cd a",
  "$ ls",
  "dir e",
  "29116 f",
  "2557 g",
  "62596 h.lst",
  "$ cd e",
  "$ ls",
  "584 i",
  "$ cd ..",
  "$ cd ..",
  "$ cd d",
  "$ ls",
  "4060174 j",
  "8033020 d.log",
  "5626152 d.ext",
  "7214296 k",
]

# class Dir:
#   def __init__(self, name):
#     self.name = name
#     self.up = None
#     self.down = None


#   def contains(self):
#     pass

# class File:
#   def __init__(self, name, size):
#     self.name = name
#     self.size = size


# class FileSystem:
#   def __init__(self):
#     self.root = None

#   def 

file_system = { #add size to this instead of dir_size
  "/": {}
}
dir_size = { 
  "/": 0,
}
list_of_dirs= set("/")
curr_dir_path = []
curr_dir = ""

# print('LENGTH !!!!!', len(input))

for command in input:
  split_command = command.split()

  # print('\n','\n', 'COMMAND', split_command)

  # FOR DIR COMMANDS
  if split_command[0] == "dir":
    key = split_command[1]

    # print("KEY KEY KEY", key)
    # print('dir_path', curr_dir_path)
    
    #to get to correct nested dictionary
    ret = file_system
    for dir in curr_dir_path:
      ret = ret[dir]
    # print(file_system)
    # print('RET - ', ret)
    
    #intantiate nested dictionary for nested directory
    ret[key] = {}
    list_of_dirs.add(key) # dont think i need?

  elif split_command[0] == "$":
    if split_command[1] == "cd":
      # move up a level of nested directory
      if split_command[2] == "..":
        curr_dir_path.pop()
        curr_dir = curr_dir_path[-1]  #change this to be tuple of entire list?
      else:
        # if split_command[2] != curr_dir:
        #   curr_dir_path.append(split_command[2])
        curr_dir_path.append(split_command[2])
        # print('curr_dir', curr_dir)
        # print('COMMAND', split_command)
        # print('DIR_PATH APPEND', split_command[2], curr_dir_path)
        curr_dir = curr_dir_path[-1] #change this to be tuple of entire list?
  
  else: # create file with size [0]
    file_name = split_command[1]
    file_size = split_command[0]
    
    # move into correctly level directory
    ret = file_system
    for dir in curr_dir_path: #NEED TO FIGURE OUT HOW TO ITERATE BACKWARDS AND USE THIS AS TUPLE FOR KEY TO LOOK UP
      ret = ret[dir]
    # print('curr dir', curr_dir)
    # print('add file', file_size, file_name)
    file = (file_name, file_size)
    ret[file] = True
    
    
    if curr_dir not in dir_size:
      dir_size[curr_dir] = 0
    dir_size[curr_dir] += int(file_size)
    # print('curr_dir_path', curr_dir_path)
    if len(curr_dir_path) > 1:
      for i in range(len(curr_dir_path)-2,-1,-1):
        if curr_dir_path[i] not in dir_size:
          dir_size[curr_dir_path[i]] = 0
        dir_size[curr_dir_path[i]] += int(file_size)

  # print(file_system, "curr_dir", curr_dir, "dir_size", dir_size)
  # print("curr_dir", curr_dir, "dir_size", dir_size)
# print('list of dirs', list_of_dirs)
  # print('\n', 'file system', file_system)
# print(file_system, "curr_dir", curr_dir)
# print('dir_size', dir_size)


res = 0
for k,v in dir_size.items():
  # print(k, v)
  if v <= 100000:
    res += v

print(res)