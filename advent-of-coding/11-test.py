from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=11)

input = lines(puzzle.input_data)


from collections import deque

monkey_items = [
  deque([79,98]),
  deque([54, 65, 75, 74]),
  deque([79, 60, 97]),
  deque([74])
]

def operation_0(x):
  return x * 19

def operation_1(x):
  return x + 6

def operation_2(x):
  return x * x

def operation_3(x):
  return x + 3

operation = {
  0: operation_0,
  1: operation_1,
  2: operation_2,
  3: operation_3
}


def test_0(x):
  if x % 23 == 0:
    return 2
  else:
    return 3

def test_1(x):
  if x % 19 == 0:
    return 2
  else:
    return 0

def test_2(x):
  if x % 13 == 0:
    return 1
  else:
    return 3

def test_3(x):
  if x % 17 == 0:
    return 0  
  else:
    return 1


test = {
  0: test_0,
  1: test_1,
  2: test_2,
  3: test_3
}

# for num, monkey in enumerate(monkey_items):
#   for item in monkey:
#     x = operation[num](item) // 3
#     print(x)

count = {
  0: 0,
  1: 0,
  2: 0,
  3: 0
}


for _ in range(20):
  for num, monkey in enumerate(monkey_items):
    while monkey:
      curr = monkey.popleft()
      count[num] += 1
      x = operation[num](curr) // 3
      # print(x)
      next_monkey = test[num](x)
      monkey_items[next_monkey].append(x)
  
# print(monkey_items, count)

largest = list(count.values())
largest.sort(reverse=True)
res = largest[0] * largest[1]
print(res)