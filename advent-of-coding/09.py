from aocd.models import Puzzle
from aocd.transforms import lines


puzzle = Puzzle(year=2022, day=9)

input = lines(puzzle.input_data)

input = [
  "R 4",
  "U 4",
  "L 3",
  "D 1",
  "R 4",
  "D 1",
  "L 5",
  "R 2"
]

start = (0,0)
head = start
tail = start
moves = {
  "L": (-1,0),
  "R": (1,0),
  "U": (0,1),
  "D": (0,-1),
  "DIAG1": (1,1),
  "DIAG2": (-1,1),
  "DIAG3": (-1,-1),
  "DIAG4": (1,-1)
}

max_x = 0
max_y = 0
min_x = 0
min_y = 0

# curr = start
# visited = set()
# for i in input:
#   print(i)
#   chunks = i.split()
#   dir = chunks[0]
#   dist = int(chunks[1])
#   # print('------', i)
#   for _ in range(dist):
#     curr_x, curr_y = curr
#     move_x, move_y = moves[dir]
#     new_head_x, new_head_y = curr_x + move_x, curr_y + move_y
#     # max_x = max(max_x, new_head_x)
#     # max_y = max(max_y, new_head_y)
#     # min_x = min(min_x, new_head_x)
#     # min_y = min(min_y, new_head_y)
#     curr = new_head_x, new_head_y
#     head = curr
#     head_x, head_y = head
#     tail_x, tail_y = tail
#     diff_x = abs(head_x - tail_x)
#     diff_y = abs(head_y - tail_y)
#     if diff_x > 1 or diff_y > 1:
#       # print('DIFF HERE')
#       for tail_dir in moves:
#         move_tail_x, move_tail_y = moves[tail_dir]
#         # potential_tail = tail_x + move_tail_x, tail_y + move_tail_y
#         # potential_x, potential_y = potential_tail
#         # print('move move', move_tail_x, move_tail_y, head_x - potential_x, head_y - potential_y)
#         if (abs(head_x - (tail_x + move_tail_x)) == 1 and abs(head_y - (tail_y + move_tail_y)) == 0) or (abs(head_x - (tail_x + move_tail_x)) == 0 and abs(head_y - (tail_y + move_tail_y)) == 1):
#           # print('hi')
#           tail = tail_x + move_tail_x, tail_y + move_tail_y
#           break
#     if tail not in visited:
#       visited.add(tail)
#     # print(head,tail)
          

#refactored
string_len = 10
string = [() for i in range(string_len)]

print(string)
# curr = start
# visited = set()
# for i in input:
#   chunks = i.split()
#   dir = chunks[0]
#   dist = int(chunks[1])
#   for _ in range(dist):
#     curr_x, curr_y = curr
#     move_x, move_y = moves[dir]
#     curr = curr_x + move_x, curr_y + move_y
#     head = curr
#     head_x, head_y = head
#     tail_x, tail_y = tail
#     diff_x = abs(head_x - tail_x)
#     diff_y = abs(head_y - tail_y)
#     if diff_x > 1 or diff_y > 1:
#       for tail_dir in moves:
#         move_tail_x, move_tail_y = moves[tail_dir]
#         if (abs(head_x - (tail_x + move_tail_x)) == 1 and abs(head_y - (tail_y + move_tail_y)) == 0) or (abs(head_x - (tail_x + move_tail_x)) == 0 and abs(head_y - (tail_y + move_tail_y)) == 1):
#           tail = tail_x + move_tail_x, tail_y + move_tail_y
#           break
#     if tail not in visited:
#       visited.add(tail)

# print(curr, min_x, min_y, max_x, max_y)
# print(len(visited))