# Write a function, uncompress, that takes in a string as an argument. The input string will be formatted into multiple groups according to the following pattern:

# <number><char>

# for example, '2c' or '3a'.

# The function should return an uncompressed version of the string where each 'char' of a group is repeated 'number' times consecutively. You may assume that the input string is well-formed according to the previously mentioned pattern.



# alex solution
# def uncompress(s):
#   res = ""
#   cur = ""
#   while i < range(len(s) - 1):
#     if s[i].isdigit():
#       cur += s[i]
#     else:
#       res += s[i]*int(cur)
#     i += 1
#   print(res)

# def uncompress(s):
#   res = ""
#   i = 0
#   j = 0 # tracking end of #s
#   while j < len(s):
#     if s[j].isalpha():
#       num = int(s[i:j])
#       res += num * s[j]
#       j += 1
#       i = j
#     else:
#       j += 1
#   print(res)

# def uncompress(s):
#   numbers = '0123456789'
#   res = ""
#   i = 0
#   j = 0 # tracking end of #s
#   while j < len(s):
#     if s[j] in numbers:
#       j += 1
#     else:
#       curr = int(s[i:j])
#       res += curr * s[j]
#       j += 1
#       i = j
      
#   return res


def uncompress(s):
  numbers = '0123456789'
  res = []
  i = 0
  j = 0 # tracking end of #s
  while j < len(s):
    if s[j] in numbers:
      j += 1
    else:
      curr = int(s[i:j])
      res.append(s[j] * curr) # better time complexity because when you add to a string, it requires looping since strings in python are immutable
      j += 1
      i = j
      
  return ''.join(res)


uncompress("2c3a1t") # -> 'ccaaat'
uncompress("4s2b") # -> 'ssssbb'
uncompress("2p1o5p") # -> 'ppoppppp'
uncompress("3n12e2z") # -> 'nnneeeeeeeeeeeezz'
uncompress("127y") # -> 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy'