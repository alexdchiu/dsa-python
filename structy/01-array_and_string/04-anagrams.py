# Write a function, anagrams, that takes in two strings as arguments. The function should return a boolean indicating whether or not the strings are anagrams. Anagrams are strings that contain the same characters, but in any order.



# alex solution
# 
#  def anagrams(s1, s2):
#   if len(s1) != len(s2):
#     return False
#   d1, d2 = {}, {}
#   for i in range(len(s1)):
#     if s1[i] not in d1:
#       d1[s1[i]] = 0
#     if s2[i] not in d2:
#       d2[s2[i]] = 0
#     d1[s1[i]] += 1
#     d2[s2[i]] += 1
#   if d1 == d2:
#     return True
#   else:
#     return False

def anagrams(s1, s2):
  print(char_counts(s1) == char_counts(s2))

def char_counts(s):
  d = {}
  for char in s:
    if char not in d:
      d[char] = 0
    d[char] += 1
  return d

anagrams('restful', 'fluster') # -> True
anagrams('cats', 'tocs') # -> False
anagrams('monkeyswrite', 'newyorktimes') # -> True
anagrams('paper', 'reapa') # -> False
anagrams('elbow', 'below') # -> True
anagrams('tax', 'taxi') # -> False
anagrams('taxi', 'tax') # -> False
anagrams('night', 'thing') # -> True
anagrams('abbc', 'aabc') # -> False
anagrams('po', 'popp') # -> false
anagrams('pp', 'oo') # -> false