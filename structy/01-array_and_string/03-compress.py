# Write a function, compress, that takes in a string as an argument. The function should return a compressed version of the string where consecutive occurrences of the same characters are compressed into the number of occurrences followed by the character. Single character occurrences should not be changed.

# 'aaa' compresses to '3a'
# 'cc' compresses to '2c'
# 't' should remain as 't'

# You can assume that the input only contains alphabetic characters.




# def compress(s):
#   s += "!"
#   i, j = 0, 0 #j is end, i is start
#   res = ""
#   while j < len(s):
#     if s[j] == s[i]:
#       j += 1
#     else:
#       num = j - i
      
#       if num == 1:
#         res += s[i]
#       else:
#         res += str(num) + s[i]
#       i = j
#   return res

def compress(s):
  s += "!"
  i, j = 0, 0 #j is end, i is start
  res = []
  while j < len(s):
    if s[j] == s[i]:
      j += 1
    else:
      num = j - i
      
      if num == 1:
        res.append(s[i])
      else:
        res.append(str(num) + s[i])
      i = j
  return ''.join(res)

compress('ccaaatsss') # -> '2c3at3s'
# compress('ssssbbz') # -> '4s2bz'
# compress('ppoppppp') # -> '2po5p'
# compress('nnneeeeeeeeeeeezz') # -> '3n12e2z'
# compress('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy'); 
# # -> '127y'