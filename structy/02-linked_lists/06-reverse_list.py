# Write a function, reverse_list, that takes in the head of a linked list as an argument. The function should reverse the order of the nodes in the linked list in-place and return the new head of the reversed linked list.

def reverse_list(head, prev=None):
  pass


# def reverse_list(head):
#   curr = head
#   prev = None
#   while curr:
#     temp = curr.next
#     curr.next = prev
#     prev = curr
#     curr = temp
#   return prev


# def reverse_list(head, prev=None):
#   if head is None:
#     return prev
#   nxt = head.next
#   head.next = prev
#   return reverse_list(nxt, head)

a = Node("a")
b = Node("b")
c = Node("c")
d = Node("d")
e = Node("e")
f = Node("f")

a.next = b
b.next = c
c.next = d
d.next = e
e.next = f

# a -> b -> c -> d -> e -> f

reverse_list(a) # f -> e -> d -> c -> b -> a

x = Node("x")
y = Node("y")

x.next = y

# x -> y

reverse_list(x) # y -> x

p = Node("p")

# p

reverse_list(p) # p