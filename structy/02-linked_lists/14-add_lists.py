# Write a function, add_lists, that takes in the head of two linked lists, each representing a number. The nodes of the linked lists contain digits as values. The nodes in the input lists are reversed; this means that the least significant digit of the number is the head. The function should return the head of a new linked listed representing the sum of the input lists. The output list should have its digits reversed as well.

# Say we wanted to compute 621 + 354 normally. The sum is 975:

#    621
#  + 354
#  -----
#    975

# Then, the reversed linked list format of this problem would appear as:

#     1 -> 2 -> 6
#  +  4 -> 5 -> 3
#  --------------
#     5 -> 7 -> 9



#   621
# + 354
# -----
#   975



class Node:
  def __init__(self, val):
    self.val = val
    self.next = None

def add_lists(head_1, head_2):
  pass # todo


def add_lists(head_1, head_2):
  curr1 = head_1
  curr2 = head_2
  dummy = Node(None)
  tail = dummy
  carry = 0
  
  while curr1 or curr2 or carry:
    val_1 = curr1.val if curr1 else 0
    val_2 = curr2.val if curr2 else 0
    curr_sum = val_1 + val_2
    if carry > 0:
      curr_sum += 1
      carry = 0
    if curr_sum > 9:
      carry += 1
      curr_sum -= 10
    
    tail.next = Node(curr_sum)
    tail = tail.next
    if curr1:
      curr1 = curr1.next
    if curr2:
      curr2 = curr2.next
  
  return dummy.next


def add_lists(head_1, head_2, carry=0):
  if head_1 is None and head_2 is None and carry == 0:
    return None
  
  val_1 = head_1.val if head_1 else 0
  val_2 = head_2.val if head_2 else 0
  
  sum = val_1 + val_2 + carry
  next_carry = 1 if sum > 9 else 0
  
  digit = sum % 10
    
  res = Node(digit)
  
  next_1 = head_1.next if head_1 else None
  next_2 = head_2.next if head_2 else None
  
  res.next = add_lists(next_1, next_2, next_carry)
  
  return res


a1 = Node(1)
a2 = Node(2)
a3 = Node(6)
a1.next = a2
a2.next = a3
# 1 -> 2 -> 6

b1 = Node(4)
b2 = Node(5)
b3 = Node(3)
b1.next = b2
b2.next = b3
# 4 -> 5 -> 3

add_lists(a1, b1)
# 5 -> 7 -> 9


#  7541
# +  32
# -----
#  7573

a1 = Node(1)
a2 = Node(4)
a3 = Node(5)
a4 = Node(7)
a1.next = a2
a2.next = a3
a3.next = a4
# 1 -> 4 -> 5 -> 7

b1 = Node(2)
b2 = Node(3)
b1.next = b2
# 2 -> 3 

add_lists(a1, b1)
# 3 -> 7 -> 5 -> 7


#   39
# + 47
# ----
#   86

a1 = Node(9)
a2 = Node(3)
a1.next = a2
# 9 -> 3

b1 = Node(7)
b2 = Node(4)
b1.next = b2
# 7 -> 4

add_lists(a1, b1)
# 6 -> 8


#   89
# + 47
# ----
#  136

a1 = Node(9)
a2 = Node(8)
a1.next = a2
# 9 -> 8

b1 = Node(7)
b2 = Node(4)
b1.next = b2
# 7 -> 4

add_lists(a1, b1)
# 6 -> 3 -> 1


#   999
#  +  6
#  ----
#  1005

a1 = Node(9)
a2 = Node(9)
a3 = Node(9)
a1.next = a2
a2.next = a3
# 9 -> 9 -> 9

b1 = Node(6)
# 6

add_lists(a1, b1)
# 5 -> 0 -> 0 -> 1