# Write a function, create_linked_list, that takes in a list of values as an argument. The function should create a linked list containing each item of the list as the values of the nodes. The function should return the head of the linked list.


class Node:
  def __init__(self, val):
    self.val = val
    self.next = None

def create_linked_list(values):
  pass # todo


# ADC solution
# def create_linked_list(values):
#   if len(values) == 0:
#     return None
  
#   head = Node(values.pop(0))
#   curr = head
  
#   while values:
#     nxt = Node(values.pop(0))
#     curr.next = nxt
#     curr = curr.next
    
#   return head


def create_linked_list(values):
  dummy = Node(None)
  tail = dummy
  
  for val in values:
    tail.next = Node(val)
    tail = tail.next
  
  return dummy.next


def create_linked_list(values):
  if len(values) == 0:
    return None

  


#  this solution not very time efficient b/c of the array slicing
#  becomes O(N^2) time complexity
def create_linked_list(values):
  if len(values) == 0:
    return None
  
  head = Node(values[0])
  
  head.next = create_linked_list(values[1:])
  
  return head


def create_linked_list(values, i = 0):
  if i == len(values):
    return None
  
  curr = Node(values[i])
  curr.next = create_linked_list(values, i + 1)
  
  return curr


create_linked_list(["h", "e", "y"])
# h -> e -> y

create_linked_list([1, 7, 1, 8])
# 1 -> 7 -> 1 -> 8

create_linked_list(["a"])
# a

create_linked_list([])
# null