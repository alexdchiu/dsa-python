# Write a function, sum_list, that takes in the head of a linked list containing numbers as an argument. The function should return the total sum of all values in the linked list.

# class Node:
#   def __init__(self, val):
#     self.val = val
#     self.next = None

def sum_list(head):
  pass


# def sum_list(head):
#   curr = head
#   res = 0
#   while curr:
#     res += curr.val
#     curr = curr.next
#   return res

# def sum_list(head):
#   if head is None:
#     return 0
#   return head.val + sum_list(head.next)




x = Node(38)
y = Node(4)

x.next = y

# 38 -> 4

sum_list(x) # 42


z = Node(100)

# 100

sum_list(z) # 100

sum_list(None) # 0