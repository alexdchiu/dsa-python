class Node:
  def __init__(self, val):
    self.val = val
    self.next = None

a = Node('A')
b = Node('B')
c = Node('C')
d = Node('D')

a.next = b
b.next = c
c.next = d

def print_list(head):
  pass

# def print_list(head):
#   curr = head
#   while curr:
#     print(curr.val)
#     curr = curr.next

# def print_list(head):
#   if head is None:
#     return
#   print(head.val)
#   print_list(head.next)

print_list(a)