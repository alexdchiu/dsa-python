# fib
# Write a function fib that takes in a number argument, n, and returns the n-th number of the Fibonacci sequence.

# The 0-th number of the sequence is 0.

# The 1-st number of the sequence is 1.

# To generate further numbers of the sequence, calculate the sum of previous two numbers.

# Solve this recursively.



##### ACHIU ITERATIVE SOLUTION
def fib(n):
  f = [0, 1]
  if n > 1:
    i = 1
    p1, p2 = 0, 1
    while i <= n:
      i += 1
      f.append(p1 + p2)
      p1 = p2
      p2 = f[i]
  return f[n]


#### ACHIU RECURSIVE BRUTE FORCE
def fib(n):
  if n == 0:
    return 0
  if n == 1:
    return 1
  return fib(n-1) + fib(n-2)

  #TIMES OUT



### STRUCTY ANSWER
def fib(n):
  return _fib(n, {})

def _fib(n, memo):
  if n in memo:
    return memo[n]
  
  if n == 0:
    return 0
  if n == 1:
    return 1
  
  memo[n] = _fib(n-1, memo) + _fib(n-2, memo)
  return memo[n]


fib(0); # -> 0
fib(1); # -> 1
fib(2); # -> 1
fib(3); # -> 2
fib(4); # -> 3
fib(5); # -> 5
fib(35); # -> 9227465
fib(46); # -> 1836311903