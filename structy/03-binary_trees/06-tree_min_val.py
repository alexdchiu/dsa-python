# Write a function, tree_min_value, that takes in the root of a binary tree that contains number values. The function should return the minimum value within the tree.

# You may assume that the input tree is non-empty.

class Node:
  def __init__(self, val):
    self.val = val
    self.left = None
    self.right = None

def tree_min_value(root):
  pass # todo




from collections import deque

def tree_min_value(root):
  queue = deque([root])
  res = float('inf')
  
  while queue:
    curr = queue.popleft()
    res = min(res, curr.val)
    if curr.left: queue.append(curr.left)
    if curr.right: queue.append(curr.right)
    
  return res


def tree_min_value(root):
  stack = [root]
  res = float('inf')
  
  while stack:
    curr = stack.pop()
    res = min(curr.val, res)
    if curr.left: stack.append(curr.left)
    if curr.right: stack.append(curr.right)
    
  return res

def tree_min_value(root):
  curr_val = root.val
  
  left_val = tree_min_value(root.left) if root.left else float('inf')
  right_val = tree_min_value(root.right) if root.right else float('inf')
  min_val = min(curr_val, left_val, right_val)
  
  return min_val


def tree_min_value(root):
  if root is None:
    return float('inf')
  
  left = tree_min_value(root.left)
  right = tree_min_value(root.right)
  
  return min(root.val, left, right)

a = Node(3)
b = Node(11)
c = Node(4)
d = Node(4)
e = Node(-2)
f = Node(1)

a.left = b
a.right = c
b.left = d
b.right = e
c.right = f

#       3
#    /    \
#   11     4
#  / \      \
# 4   -2     1
tree_min_value(a) # -> -2


a = Node(5)
b = Node(11)
c = Node(3)
d = Node(4)
e = Node(14)
f = Node(12)

a.left = b
a.right = c
b.left = d
b.right = e
c.right = f

#       5
#    /    \
#   11     3
#  / \      \
# 4   14     12

tree_min_value(a) # -> 3


a = Node(-1)
b = Node(-6)
c = Node(-5)
d = Node(-3)
e = Node(-4)
f = Node(-13)
g = Node(-2)
h = Node(-2)

a.left = b
a.right = c
b.left = d
b.right = e
c.right = f
e.left = g
f.right = h

#        -1
#      /   \
#    -6    -5
#   /  \     \
# -3   -4   -13
#     /       \
#    -2       -2

tree_min_value(a) # -> -13


a = Node(42)

#        42

tree_min_value(a) # -> 42