# Write a function, tree_levels, that takes in the root of a binary tree. The function should return a 2-Dimensional list where each sublist represents a level of the tree.


class Node:
  def __init__(self, val):
    self.val = val
    self.left = None
    self.right = None

def tree_levels(root):
  pass # todo

from collections import deque

def tree_levels(root):
  if root is None:
    return []
  
  queue = deque([(root, 0)])
  
  res = []
  curr_level = 0
  curr_level_list = []
  while queue:
    curr, lvl = queue.popleft()
    
    if lvl > curr_level:
      res.append(curr_level_list)
      curr_level = lvl
      curr_level_list = []
    
    if lvl == curr_level:
      curr_level_list.append(curr.val)
    
    if curr.left:
      queue.append((curr.left, lvl+1))
    if curr.right:
      queue.append((curr.right, lvl+1))
  
  res.append(curr_level_list)
  
  return res


def tree_levels(root):
  if root is None:
    return []
  
  levels = []
  
  stack = [(root, 0)]
  
  while stack:
    curr, lvl = stack.pop()
    
    if len(levels) == lvl:
      levels.append([curr.val])
    else:
      levels[lvl].append(curr.val)
    
    if curr.right:
      stack.append((curr.right, lvl+1))
    
    if curr.left:
      stack.append((curr.left, lvl+1))
    
  return levels


def tree_levels(root):
  levels = []
  fill_levels(root, levels, 0)
  return levels

def fill_levels(root, levels, lvl_num):
  if root is None:
    return 
  if len(levels) == lvl_num:
    levels.append([root.val])
  else:
    levels[lvl_num].append(root.val)
  fill_levels(root.left, levels, lvl_num+1)
  fill_levels(root.right, levels, lvl_num+1)


a = Node('a')
b = Node('b')
c = Node('c')
d = Node('d')
e = Node('e')
f = Node('f')

a.left = b
a.right = c
b.left = d
b.right = e
c.right = f

#      a
#    /   \
#   b     c
#  / \     \
# d   e     f

tree_levels(a) # ->
# [
#   ['a'],
#   ['b', 'c'],
#   ['d', 'e', 'f']
# ]

a = Node('a')
b = Node('b')
c = Node('c')
d = Node('d')
e = Node('e')
f = Node('f')
g = Node('g')
h = Node('h')
i = Node('i')

a.left = b
a.right = c
b.left = d
b.right = e
c.right = f
e.left = g
e.right = h
f.left = i

#         a
#      /    \
#     b      c
#   /  \      \
#  d    e      f
#      / \    /
#     g  h   i

tree_levels(a) # ->
# [
#   ['a'],
#   ['b', 'c'],
#   ['d', 'e', 'f'],
#   ['g', 'h', 'i']
# ]


q = Node('q')
r = Node('r')
s = Node('s')
t = Node('t')
u = Node('u')
v = Node('v')

q.left = r
q.right = s
r.right = t
t.left = u
u.right = v

#      q
#    /   \
#   r     s
#    \
#     t
#    /
#   u
#  /
# v

tree_levels(q) # ->
# [
#   ['q'],
#   ['r', 's'],
#   ['t'],
#   ['u'],
#   ['v']
# ]


tree_levels(None) # -> []