# Write a function, connected_components_count, that takes in the adjacency list of an undirected graph. The function should return the number of connected components within the graph.

def connected_components_count(graph):
  pass # todo

#BFS
from collections import deque

def connected_components_count(graph):
  count = 0
  visited = set()
  for node in graph:
    if explore(node, graph, visited) == True:
      count += 1
    
  return count


def explore(node, graph, visited):
  if node in visited:
    return False
  
  queue = deque([node])
  
  while queue:
    curr = queue.popleft()
    if curr in visited:
      continue
    visited.add(curr)
    
    for neighbor in graph[curr]:
      queue.append(neighbor)
  
  return True


#DFS
def connected_components_count(graph):
  count = 0
  visited = set()
  
  for node in graph:
    if explore(node, graph, visited) == True:
      count += 1
    
  return count

def explore(node, graph, visited):
  if node in visited:
    return False
  
  stack = [node]
  
  while stack:
    curr = stack.pop()
    
    if curr in visited:
      continue
      
    visited.add(curr)
    
    for neighbor in graph[curr]:
      stack.append(neighbor)
      
  return True


def connected_components_count(graph):
  count = 0
  visited = set()
  
  for node in graph:
    if explore(node, graph, visited) == True:
      count += 1
    
  return count

def explore(node, graph, visited):
  if node in visited:
    return False
  
  visited.add(node)
  
  for neighbor in graph[node]:
    explore(neighbor, graph, visited)
      
  return True


connected_components_count({
  0: [8, 1, 5],
  1: [0],
  5: [0, 8],
  8: [0, 5],
  2: [3, 4],
  3: [2, 4],
  4: [3, 2]
}) # -> 2



connected_components_count({
  1: [2],
  2: [1,8],
  6: [7],
  9: [8],
  7: [6, 8],
  8: [9, 7, 2]
}) # -> 1



connected_components_count({
  3: [],
  4: [6],
  6: [4, 5, 7, 8],
  8: [6],
  7: [6],
  5: [6],
  1: [2],
  2: [1]
}) # -> 3


connected_components_count({}) # -> 0


connected_components_count({
  0: [4,7],
  1: [],
  2: [],
  3: [6],
  4: [0],
  6: [3],
  7: [0],
  8: []
}) # -> 5