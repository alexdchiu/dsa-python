# Write a function, prereqs_possible, that takes in a number of courses (n) and prerequisites as arguments. Courses have ids ranging from 0 through n - 1. A single prerequisite of (A, B) means that course A must be taken before course B. The function should return a boolean indicating whether or not it is possible to complete all courses.




def prereqs_possible(num_courses, prereqs):
  pass # todo




########## ACHIU SOLUTION

# map out courses + build adj list for prereqs
# detect if there is a cycle - if so, impossible to take all the courses


def prereqs_possible(num_courses, prereqs):
  visiting = set()
  visited = set()
  
  graph = build_graph(num_courses, prereqs)
  
  for node in graph:
    if detect_cycle(graph, node, visiting, visited) == True:
      return False
  return True
  
def build_graph(num_courses, prereqs):
  graph = {}
  for i in range(num_courses):
    graph[i] = []
  for p in prereqs:
    p1, p2 = p
    graph[p1].append(p2)
  return graph
  
def detect_cycle(graph, node, visiting, visited):
  if node in visiting:
    return True
  if node in visited:
    return False
  
  visiting.add(node)
  
  for neighbor in graph[node]:
    if detect_cycle(graph, neighbor, visiting, visited):
      return True
  
  visiting.remove(node)
  visited.add(node)
  
  return False


numCourses = 6
prereqs = [
  (0, 1),
  (2, 3),
  (0, 2),
  (1, 3),
  (4, 5),
]
prereqs_possible(numCourses, prereqs) # -> True


numCourses = 6
prereqs = [
  (0, 1),
  (2, 3),
  (0, 2),
  (1, 3),
  (4, 5),
  (3, 0),
]
prereqs_possible(numCourses, prereqs) # -> False


numCourses = 5
prereqs = [
  (2, 4),
  (1, 0),
  (0, 2),
  (0, 4),
]
prereqs_possible(numCourses, prereqs) # -> True


numCourses = 6
prereqs = [
  (2, 4),
  (1, 0),
  (0, 2),
  (0, 4),
  (5, 3),
  (3, 5),
]
prereqs_possible(numCourses, prereqs) # -> False


numCourses = 8
prereqs = [
  (1, 0),
  (0, 6),
  (2, 0),
  (0, 5),
  (3, 7),
  (4, 3),
]
prereqs_possible(numCourses, prereqs) # -> True


numCourses = 8
prereqs = [
  (1, 0),
  (0, 6),
  (2, 0),
  (0, 5),
  (3, 7),
  (7, 4),
  (4, 3),
]
prereqs_possible(numCourses, prereqs) # -> False


numCourses = 42
prereqs = [(6, 36)]
prereqs_possible(numCourses, prereqs) # -> True