# Write a function, undirected_path, that takes in a list of edges for an undirected graph and two nodes (node_A, node_B). The function should return a boolean indicating whether or not there exists a path between node_A and node_B.

def undirected_path(edges, node_A, node_B):
  pass # todo


#BFS
from collections import deque

def undirected_path(edges, node_A, node_B):
  neighbors = adjacency_list(edges)
  visited = set()
  queue = deque([node_A])
  
  while queue:
    curr = queue.popleft()
    visited.add(curr)
    if curr == node_B:
      return True
    
    for neighbor in neighbors[curr]:
      if neighbor not in visited:
        queue.append(neighbor)
      
  return False

    
def adjacency_list(edges):
  neighbors = {}
  for edge in edges:
    first, second = edge
    if first not in neighbors:
      neighbors[first] = []
    if second not in neighbors:
      neighbors[second] = []
    neighbors[first].append(second)
    neighbors[second].append(first)
    
  return neighbors


#DFS
def undirected_path(edges, node_A, node_B):
  neighbors = adjacency_list(edges)
  visited = set()
  stack = [node_A]
  
  while stack:
    curr = stack.pop()
    visited.add(curr)
    if curr == node_B:
      return True
    
    for neighbor in neighbors[curr]:
      if neighbor not in visited:
        stack.append(neighbor)
      
  return False

    
def adjacency_list(edges):
  neighbors = {}
  for edge in edges:
    first, second = edge
    if first not in neighbors:
      neighbors[first] = []
    if second not in neighbors:
      neighbors[second] = []
    neighbors[first].append(second)
    neighbors[second].append(first)
    
  return neighbors



#DFS RECURSIVE
def undirected_path(edges, node_A, node_B):
  neighbors = {}
  visited = set()
  for edge in edges:
    first, second = edge
    if first not in neighbors:
      neighbors[first] = []
    if second not in neighbors:
      neighbors[second] = []
    neighbors[first].append(second)
    neighbors[second].append(first)
  
  return has_path(neighbors, node_A, node_B, visited)
  
  
def has_path(neighbors, src, dst, visited):
  if src == dst:
    return True
  
  if src in visited:
    return False
  
  visited.add(src)
  
  for neighbor in neighbors[src]:
    if has_path(neighbors, neighbor, dst, visited) == True:
      return True
  
  return False

edges = [
  ('i', 'j'),
  ('k', 'i'),
  ('m', 'k'),
  ('k', 'l'),
  ('o', 'n')
]

undirected_path(edges, 'j', 'm') # -> True


edges = [
  ('i', 'j'),
  ('k', 'i'),
  ('m', 'k'),
  ('k', 'l'),
  ('o', 'n')
]

undirected_path(edges, 'm', 'j') # -> True


edges = [
  ('i', 'j'),
  ('k', 'i'),
  ('m', 'k'),
  ('k', 'l'),
  ('o', 'n')
]

undirected_path(edges, 'l', 'j') # -> True


edges = [
  ('i', 'j'),
  ('k', 'i'),
  ('m', 'k'),
  ('k', 'l'),
  ('o', 'n')
]

undirected_path(edges, 'k', 'o') # -> False


edges = [
  ('i', 'j'),
  ('k', 'i'),
  ('m', 'k'),
  ('k', 'l'),
  ('o', 'n')
]

undirected_path(edges, 'i', 'o') # -> False


edges = [
  ('b', 'a'),
  ('c', 'a'),
  ('b', 'c'),
  ('q', 'r'),
  ('q', 's'),
  ('q', 'u'),
  ('q', 't'),
]


undirected_path(edges, 'a', 'b') # -> True


edges = [
  ('b', 'a'),
  ('c', 'a'),
  ('b', 'c'),
  ('q', 'r'),
  ('q', 's'),
  ('q', 'u'),
  ('q', 't'),
]

undirected_path(edges, 'a', 'c') # -> True


edges = [
  ('b', 'a'),
  ('c', 'a'),
  ('b', 'c'),
  ('q', 'r'),
  ('q', 's'),
  ('q', 'u'),
  ('q', 't'),
]

undirected_path(edges, 'r', 't') # -> True


edges = [
  ('b', 'a'),
  ('c', 'a'),
  ('b', 'c'),
  ('q', 'r'),
  ('q', 's'),
  ('q', 'u'),
  ('q', 't'),
]

undirected_path(edges, 'r', 'b') # -> False


edges = [
  ('s', 'r'),
  ('t', 'q'),
  ('q', 'r'),
];

undirected_path(edges, 'r', 't'); # -> True