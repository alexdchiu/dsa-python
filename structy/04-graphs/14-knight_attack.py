# A knight and a pawn are on a chess board. Can you figure out the minimum number of moves required for the knight to travel to the same position of the pawn? On a single move, the knight can move in an "L" shape; two spaces in any direction, then one space in a perpendicular direction. This means that on a single move, a knight has eight possible positions it can move to.

# Write a function, knight_attack, that takes in 5 arguments:

# n, kr, kc, pr, pc

# n = the length of the chess board
# kr = the starting row of the knight
# kc = the starting column of the knight
# pr = the row of the pawn
# pc = the column of the pawn
# The function should return a number representing the minimum number of moves required for the knight to land ontop of the pawn. The knight cannot move out-of-bounds of the board. You can assume that rows and columns are 0-indexed. This means that if n = 8, there are 8 rows and 8 columns numbered 0 to 7. If it is not possible for the knight to attack the pawn, then return None.

def knight_attack(n, kr, kc, pr, pc):
  pass # todo



from collections import deque


##### ACHIU SOLUTION

def knight_attack(n, kr, kc, pr, pc):
  k_pos = (kr, kc)
  p_pos = (pr, pc)
  visited = set()
  
  k_moves = [
    (-2, -1),
    (-2, 1),
    (-1, -2),
    (-1, 2),
    (1, -2),
    (1, 2),
    (2, -1),
    (2, 1)
  ]
  
  queue = deque([(k_pos, 0)])
  
  while queue:
    curr_k_pos, moves = queue.popleft()
    if curr_k_pos == p_pos:
      return moves
    
    if curr_k_pos in visited:
      continue
      
    visited.add(curr_k_pos)
    
    r, c = curr_k_pos
    
    for move in k_moves:
      kr, kc = move
      new_r, new_c = r + kr, c + kc
      new_pos = (new_r, new_c)
      r_inbounds = 0 <= new_r < n
      c_inbounds = 0 <= new_c < n
      if r_inbounds and c_inbounds and new_pos not in visited:
        queue.append((new_pos, moves + 1))
  
  return None



##### STRUCTY SOLUTION
from collections import deque

def knight_attack(n, kr, kc, pr, pc):
  visited = set();
  visited.add((kr, kc))
  queue = deque([ (kr, kc, 0) ])
  while len(queue) > 0:
    r, c, step = queue.popleft();
    if (r, c) == (pr, pc):
      return step
    neighbors = get_knight_moves(n, r, c)
    for neighbor in neighbors:
      neighbor_row, neighbor_col = neighbor
      if neighbor not in visited:
        visited.add(neighbor)
        queue.append((neighbor_row, neighbor_col, step + 1))
  return None

def get_knight_moves(n, r, c):
  positions = [
    ( r + 2, c + 1 ),
    ( r - 2, c + 1 ),
    ( r + 2, c - 1 ),
    ( r - 2, c - 1 ),
    ( r + 1, c + 2 ),
    ( r - 1, c + 2 ),
    ( r + 1, c - 2 ),
    ( r - 1, c - 2 ),
  ]
  inbounds_positions = [];
  for pos in positions:
    new_row, new_col = pos
    if 0 <= new_row < n and 0 <= new_col < n:
      inbounds_positions.append(pos)
  return inbounds_positions


####### ACHIU REFACTORED

from collections import deque

def knight_attack(n, kr, kc, pr, pc):
  k_pos = (kr, kc)
  p_pos = (pr, pc)
  visited = set()
  
  
  queue = deque([(k_pos, 0)])
  
  while queue:
    curr_k_pos, moves = queue.popleft()
    if curr_k_pos == p_pos:
      return moves
    
    if curr_k_pos in visited:
      continue
      
    visited.add(curr_k_pos)
    
    k_moves = knight_moves(n, curr_k_pos)
    
    for neighbor in k_moves:
      if neighbor not in visited:
        queue.append((neighbor, moves + 1))
  
  return None


def knight_moves(n, curr):
  r, c = curr
  k_moves = [
      (-2, -1),
      (-2, 1),
      (-1, -2),
      (-1, 2),
      (1, -2),
      (1, 2),
      (2, -1),
      (2, 1)
    ]
  inbounds_positions = []
  
  for move in k_moves:
    kr, kc = move
    new_r, new_c = r + kr, c + kc
    new_pos = (new_r, new_c)
    r_inbounds = 0 <= new_r < n
    c_inbounds = 0 <= new_c < n
    if r_inbounds and c_inbounds:
      inbounds_positions.append(new_pos)
  
  return inbounds_positions




knight_attack(8, 1, 1, 2, 2) # -> 2

knight_attack(8, 1, 1, 2, 3) # -> 1

knight_attack(8, 0, 3, 4, 2) # -> 3

knight_attack(8, 0, 3, 5, 2) # -> 4

knight_attack(24, 4, 7, 19, 20) # -> 10

knight_attack(100, 21, 10, 0, 0) # -> 11

knight_attack(3, 0, 0, 1, 2) # -> 1

knight_attack(3, 0, 0, 1, 1) # -> None