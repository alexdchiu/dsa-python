# Write a function, largest_component, that takes in the adjacency list of an undirected graph. The function should return the size of the largest connected component in the graph.

def largest_component(graph):
  pass # todo


#BFS
from collections import deque

def largest_component(graph):
  largest = 0
  visited = set()
  for node in graph:
    largest = max(largest, explore(node, graph, visited))
    
  return largest

def explore(node, graph, visited):
  if node in visited:
    return 0
  
  size = 0
  
  queue = deque([node])
  while queue:
    curr = queue.popleft()
    if curr in visited:
      continue
      
    visited.add(curr)
    size += 1
    
    for neighbor in graph[curr]:
      queue.append(neighbor)
      
  return size
  





#DFS
def largest_component(graph):
  visited = set()
  largest = 0
  
  for node in graph:
    largest = max(largest, explore(node, graph, visited))
    
  return largest

def explore(node, graph, visited):
  if node in visited:
    return 0
  
  size = 0
  stack = [node]
  while stack:
    curr = stack.pop()
    if curr in visited:
      continue
    
    visited.add(curr)
    size += 1
    for neighbor in graph[curr]:
      stack.append(neighbor)
  
  return size



#DFS RECURSIVE

def largest_component(graph):
  visited = set()
  largest = 0
  
  for node in graph:
    largest = max(largest, explore(node, graph, visited))
    
  return largest

def explore(node, graph, visited):
  if node in visited:
    return 0
  
  visited.add(node)
  size = 1
  for neighbor in graph[node]:
    size += explore(neighbor, graph, visited)
  
  return size


largest_component({
  0: [8, 1, 5],
  1: [0],
  5: [0, 8],
  8: [0, 5],
  2: [3, 4],
  3: [2, 4],
  4: [3, 2]
}) # -> 4


largest_component({
  1: [2],
  2: [1,8],
  6: [7],
  9: [8],
  7: [6, 8],
  8: [9, 7, 2]
}) # -> 6


largest_component({
  3: [],
  4: [6],
  6: [4, 5, 7, 8],
  8: [6],
  7: [6],
  5: [6],
  1: [2],
  2: [1]
}) # -> 5


largest_component({}) # -> 0

largest_component({
  0: [4,7],
  1: [],
  2: [],
  3: [6],
  4: [0],
  6: [3],
  7: [0],
  8: []
}) # -> 3