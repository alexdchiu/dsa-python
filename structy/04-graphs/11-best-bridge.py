# Write a function, best_bridge, that takes in a grid as an argument. The grid contains water (W) and land (L). There are exactly two islands in the grid. An island is a vertically or horizontally connected region of land. Return the minimum length bridge needed to connect the two islands. A bridge does not need to form a straight line.

def best_bridge(grid):
  pass # todo

######## ACHIU SOLUTION
from collections import deque

def best_bridge(grid):
  first = set()
  bridge = 0
  loop = True
  for r in range(len(grid)):
    if loop:
      for c in range(len(grid[0])):
        if loop:
          if grid[r][c] == "L":
            curr = (r,c)
            explore(grid, curr, first)
            loop = False
  
  visited = set()
  
  queue = deque([])
  
  for pos in first:
    queue.append((pos,0))
    
  # print(queue)
  while queue:
    curr, dist = queue.popleft()
    if curr in visited:
      continue
    
    visited.add(curr)
    print(curr)
    r,c = curr
    if grid[r][c] == "L" and curr not in first:
      return dist-1
    
    neighbors = [
      (1,0),
      (-1,0),
      (0,1),
      (0,-1)
    ]
    
    for neighbor in neighbors:
      nr, nc = neighbor
      new_r, new_c = r + nr, c + nc
      new_pos = (new_r, new_c)
      r_inbounds = 0 <= new_r < len(grid)
      c_inbounds = 0 <= new_c < len(grid[0])
      
      if r_inbounds and c_inbounds and new_pos not in visited:
        queue.append((new_pos,dist+1))
    
    
        
        
def explore(grid, curr, first):
  r, c = curr
  
  r_inbounds = 0 <= r < len(grid)
  c_inbounds = 0 <= c < len(grid[0])
  
  if not r_inbounds or not c_inbounds:
    return
  
  if grid[r][c] == "W":
    return
  
  if curr in first:
    return
  
  first.add(curr)
  
  neighbors = [
    (1,0),
    (-1,0),
    (0,1),
    (0,-1)
  ]
  
  for neighbor in neighbors:
    nr, nc = neighbor
    new_r, new_c = r + nr, c + nc
    new_pos = (new_r, new_c)
    explore(grid, new_pos, first)
  
  return

grid = [
  ["W", "W", "W", "L", "L"],
  ["L", "L", "W", "W", "L"],
  ["L", "L", "L", "W", "L"],
  ["W", "L", "W", "W", "W"],
  ["W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W"],
]
best_bridge(grid) # -> 1

grid = [
  ["W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W"],
  ["L", "L", "W", "W", "L"],
  ["W", "L", "W", "W", "L"],
  ["W", "W", "W", "L", "L"],
  ["W", "W", "W", "W", "W"],
]
best_bridge(grid) # -> 2

grid = [
  ["W", "W", "W", "W", "W"],
  ["W", "W", "W", "L", "W"],
  ["L", "W", "W", "W", "W"],
]
best_bridge(grid) # -> 3


grid = [
  ["W", "W", "W", "W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W", "L", "W", "W"],
  ["W", "W", "W", "W", "L", "L", "W", "W"],
  ["W", "W", "W", "W", "L", "L", "L", "W"],
  ["W", "W", "W", "W", "W", "L", "L", "L"],
  ["L", "W", "W", "W", "W", "L", "L", "L"],
  ["L", "L", "L", "W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W", "W", "W", "W"],
]
best_bridge(grid) # -> 3

grid = [
  ["L", "L", "L", "L", "L", "L", "L", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "W", "W", "L", "W", "W", "W", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "W", "W", "W", "W", "W", "W", "L"],
  ["L", "L", "L", "L", "L", "L", "L", "L"],
]
best_bridge(grid) # -> 2


grid = [
  ["W", "L", "W", "W", "W", "W", "W", "W"],
  ["W", "L", "W", "W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W", "W", "W", "W"],
  ["W", "W", "W", "W", "W", "W", "L", "W"],
  ["W", "W", "W", "W", "W", "W", "L", "L"],
  ["W", "W", "W", "W", "W", "W", "W", "L"],
]
best_bridge(grid) # -> 8