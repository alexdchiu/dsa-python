# Write a function, semesters_required, that takes in a number of courses (n) and a list of prerequisites as arguments. Courses have ids ranging from 0 through n - 1. A single prerequisite of (A, B) means that course A must be taken before course B. Return the minimum number of semesters required to complete all n courses. There is no limit on how many courses you can take in a single semester, as long the prerequisites of a course are satisfied before taking it.

# Note that given prerequisite (A, B), you cannot take course A and course B concurrently in the same semester. You must take A in some semester before B.

# You can assume that it is possible to eventually complete all courses.

######## ACHIU ANSWER:
def semesters_required(num_courses, prereqs):
  if len(prereqs) == 0 and num_courses:
    return 1
  
  adj_list = graph(prereqs)
  num_of_sems = {}
  for node in adj_list:
    if len(adj_list[node]) == 0:
      num_of_sems[node] = 1
  
  for node in adj_list:
    explore(node, adj_list, num_of_sems)
  vals = num_of_sems.values()
  return max(num_of_sems.values())
  
def graph(lst):
  graph = {}
  for l in lst:
    l1, l2 = l
    if l1 not in graph:
      graph[l1] = []
    if l2 not in graph:
      graph[l2] = []
    graph[l1].append(l2)
  return graph

def explore(node, adj_list, num_of_sems):
  if node in num_of_sems:
    return num_of_sems[node]
  
  sems = 0
  for neighbor in adj_list[node]:
    neighbor_distance = explore(neighbor, adj_list, num_of_sems)
    sems = max(neighbor_distance, sems)
  num_of_sems[node] = 1 + sems
  return num_of_sems[node]

######## STRUCTY ANSWER:

def semesters_required(num_courses, prereqs):
  adj_list = graph(num_courses, prereqs)
  print(adj_list)
  num_of_sems = {}
  for node in range(num_courses):
    if len(adj_list[node]) == 0:
      num_of_sems[node] = 1
  
  for node in adj_list:
    explore(node, adj_list, num_of_sems)
  vals = num_of_sems.values()
  return max(vals)
  
def graph(num_courses, prereqs):
  graph = {}
  for course in range(num_courses):
    graph[course] = []
  
  for prereq in prereqs:
    a,b = prereq
    graph[a].append(b)
  
  return graph

def explore(node, adj_list, num_of_sems):
  if node in num_of_sems:
    return num_of_sems[node]
  
  sems = 0
  for neighbor in adj_list[node]:
    neighbor_distance = explore(neighbor, adj_list, num_of_sems)
    sems = max(neighbor_distance, sems)
  num_of_sems[node] = 1 + sems
  return num_of_sems[node]



num_courses = 6
prereqs = [
  (1, 2),
  (2, 4),
  (3, 5),
  (0, 5),
]
semesters_required(num_courses, prereqs) # -> 3


num_courses = 7
prereqs = [
  (4, 3),
  (3, 2),
  (2, 1),
  (1, 0),
  (5, 2),
  (5, 6),
]
semesters_required(num_courses, prereqs) # -> 5


num_courses = 5
prereqs = [
  (1, 0),
  (3, 4),
  (1, 2),
  (3, 2),
]
semesters_required(num_courses, prereqs) # -> 2


num_courses = 12
prereqs = []
semesters_required(num_courses, prereqs) # -> 1


num_courses = 3
prereqs = [
  (0, 2),
  (0, 1),
  (1, 2),
]
semesters_required(num_courses, prereqs) # -> 3


num_courses = 6
prereqs = [
  (3, 4),
  (3, 0),
  (3, 1),
  (3, 2),
  (3, 5),
]
semesters_required(num_courses, prereqs) # -> 2