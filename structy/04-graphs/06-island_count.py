# Write a function, island_count, that takes in a grid containing Ws and Ls. W represents water and L represents land. The function should return the number of islands on the grid. An island is a vertically or horizontally connected region of land.


def island_count(grid):
  pass # todo


def island_count(grid):
  count = 0
  visited = set()
  for r in range(len(grid)):
    for c in range(len(grid[0])):
      if explore(grid, r, c, visited) == True:
        count += 1
  
  return count


def explore(grid, r, c, visited):
  r_inbounds = 0 <= r < len(grid)
  c_inbounds = 0 <= c < len(grid[0])
  
  if not r_inbounds or not c_inbounds:
    return False
  
  
  if (r,c) in visited:
    return False
  
  if grid[r][c] == "W":
    return False
  
  
  visited.add((r,c))
  
  explore(grid, r+1, c, visited)
  explore(grid, r-1, c, visited)
  explore(grid, r, c+1, visited)
  explore(grid, r, c-1, visited)
  
  return True




grid = [
  ['W', 'L', 'W', 'W', 'W'],
  ['W', 'L', 'W', 'W', 'W'],
  ['W', 'W', 'W', 'L', 'W'],
  ['W', 'W', 'L', 'L', 'W'],
  ['L', 'W', 'W', 'L', 'L'],
  ['L', 'L', 'W', 'W', 'W'],
]

island_count(grid) # -> 3

grid = [
  ['L', 'W', 'W', 'L', 'W'],
  ['L', 'W', 'W', 'L', 'L'],
  ['W', 'L', 'W', 'L', 'W'],
  ['W', 'W', 'W', 'W', 'W'],
  ['W', 'W', 'L', 'L', 'L'],
]

island_count(grid) # -> 4

grid = [
  ['L', 'L', 'L'],
  ['L', 'L', 'L'],
  ['L', 'L', 'L'],
]

island_count(grid) # -> 1

grid = [
  ['W', 'W'],
  ['W', 'W'],
  ['W', 'W'],
]

island_count(grid) # -> 0