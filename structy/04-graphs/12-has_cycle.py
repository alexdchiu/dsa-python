# Write a function, has_cycle, that takes in an object representing the adjacency list of a directed graph. The function should return a boolean indicating whether or not the graph contains a cycle.

def has_cycle(graph):
  pass # todo

### ACHIU's attempt. failes test 5
def has_cycle(graph):
  for node in graph:
    visited = set()
    
    stack = [node]
    
    while stack:
      curr = stack.pop()
      # print(curr)
      visited.add(curr)
      for neighbor in graph[curr]:
        if neighbor == node:
          return True
        stack.append(neighbor)
    
  return False


##### STRUCTY
# white: unexplored
# grey: visitng
# black: visited - no neighbors or all neighbors are also black
# if you traverse back to a node that is gray / in progress / visiting, means that you must have a cycle.


def has_cycle(graph):
  visited = set()
  visiting = set()
  for node in graph:
    if cycle_detect(graph, node, visiting, visited) == True:
      return True
  return False

def cycle_detect(graph, node, visiting, visited):
  if node in visited:
    return False
  
  if node in visiting:
    return True
  
  visiting.add(node)
  
  for neighbor in graph[node]:
    if cycle_detect(graph, neighbor, visiting, visited) == True:
      return True
  
  visited.add(node)
  visiting.remove(node)
  
  return False  


has_cycle({
  "a": ["b"],
  "b": ["c"],
  "c": ["a"],
}) # -> True



has_cycle({
  "a": ["b", "c"],
  "b": ["c"],
  "c": ["d"],
  "d": [],
}) # -> False


has_cycle({
  "a": ["b", "c"],
  "b": [],
  "c": [],
  "e": ["f"],
  "f": ["e"],
}) # -> True


has_cycle({
  "q": ["r", "s"],
  "r": ["t", "u"],
  "s": [],
  "t": [],
  "u": [],
  "v": ["w"],
  "w": [],
  "x": ["w"],
}) # -> False


has_cycle({
  "a": ["b"],
  "b": ["c"],
  "c": ["a"],
  "g": [],
}) # -> True


has_cycle({
  "a": ["b"],
  "b": ["c"],
  "c": ["d"],
  "d": ["b"],
}) # -> True